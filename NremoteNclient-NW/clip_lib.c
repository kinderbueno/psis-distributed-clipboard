#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "clip_lib.h"
#include "ext_var.h"

/* external variables */
char * clip[10];
int root_fd;
int in_fd;
int out_fd;
node * head_clip;
node * head_clients;

pthread_rwlock_t clip_lock[10];
pthread_rwlock_t hclip_lock; 
pthread_rwlock_t hclient_lock;
pthread_rwlock_t root_lock; 

int check_args(int c, char* v[])
{
	char ip[30];
	int err=0, port=0;
	struct sockaddr_in root_addr;	

	memset(&root_addr, (int) '\0', sizeof(root_addr));
	memset(ip, (int) '\0', sizeof(char)*30);
	root_fd=-1;

	if (c==1)
		return 0; /* clipboard is alone */
	else if (c==4)
	{
		/* scans through the arguments and verifies them before adding to the stucture 	*/
		if(!strcmp("-c", v[1]))
		{
			if(sscanf(v[2], "%s", ip)!=1 || !inet_aton(ip, &(root_addr.sin_addr)))
			{
				printf("Invalid given IP. Not connecting\n");
				return -1;
			}
			if(sscanf(v[3], "%d", &port)!=1 || !(port>=MINPORT && port<=MAXPORT) )
			{
				printf("Invalid given port. Not connecting. It should be a integer between %d and %d.\n", MINPORT, MAXPORT);
				return -1;
			}

			/* arguments are now verified  */
			root_addr.sin_family = AF_INET;
			root_addr.sin_port = htons((u_short) port);
			/* central_server->sin_addr.s_addr = server_ip.s_addr; ---> isto ja esta feito acima */

			root_fd = socket(AF_INET, SOCK_STREAM, 0);
			if (root_fd == -1){
				perror("socket: ");
				return -1;
			}

			err = connect(root_fd,(struct sockaddr *)&root_addr, sizeof(root_addr));
			if(err==-1)
			{
				printf("Error connecting to next clipboard. Not connecting.\n");
				close(root_fd);
				return -1;
			}
			return root_fd; 
		}
		else 
		{
			printf("Invalid arguments. Not connecting. Use the following:\n./clipboard [-c ip_next port_next]\n");
			return -1;
		}	
	}
	else
	{
		printf("Invalid arguments. Not connecting. Use the following:\n./clipboard [-c ip_next port_next]\n");
		return -1;
	}
}

int fetch_clip(int fd)
{
	char default_messages[10];
	int size=0;
	int i=0, j=0, nbytes=0;
	
	memset(default_messages, (int) '\0', sizeof(char)*10);
	/* send fetch request */
	sprintf(default_messages, "%d:%d", OPER_CODE, FETCH_CODE);
	nbytes = send(fd, default_messages, sizeof(char)*10, 0);
	if (nbytes<=0) /* (nbytes<10)*/
	{
		printf("Unable to send message.\n");
		return 0;
	}
	for(i=0;i<10;i++)
	{
		memset(default_messages, (int)'\0',sizeof(char)*10);
		nbytes = recv(fd, default_messages, sizeof(char)*10, 0); /* n de bytes */
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The clip has exited unexpectadly. %d\n", nbytes);
			return 0;
		}
		sscanf(default_messages,"%d" ,&size);
		if(size==0)
			continue;
		if((clip[i]=(char*)malloc(sizeof(char)*(size+1)))==NULL)
		{
			for(j=0;j<i;j++)
			{
				free(clip[j]);
				clip[j]=NULL;
			}
			return 0;
		}
		memset(clip[i], (int) '\0', sizeof(char)*(size+1));
		nbytes = recv(fd, clip[i] , sizeof(char)*size, 0); /* n de bytes */
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
			for(j=0;j<i;j++)
			{
				free(clip[j]);
				clip[j]=NULL;
			}
			return 0;
		}
	}
	return 1;
}

void * thread_root(void * arg)
{
	int * fd = (int *) arg;
	int nbytes=0, region=0, size=0;
	char* aux_clip=NULL, *aux_free=NULL;
	char default_messages[10];

	/* ja tenho os clips sincronizados */
	while(1)
	{
		nbytes=recv(*fd, default_messages, 10*sizeof(char),0);
		if (nbytes==0)
		{
			printf("Passando para modo stand alone.\n");
			pthread_rwlock_wrlock(&root_lock);
			close(*fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			break;
		}
		sscanf(default_messages, "%d:%d", &region, &size);
		if(region<10 || region>=0)
		{
			if (size>0)
			{
				/* este malloc esta protegido para remover a ligaçao com o Clipboard acima pois fica num estado inconsistente */
				if((aux_clip=(char*)malloc(sizeof(char)*(size+1)))==NULL) 
				{
					printf("Malloc Error. Change to Stand Alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Passando para modo stand alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				memset(aux_clip, (int) '\0', (size+1)*sizeof(char));
				nbytes=recv(*fd, aux_clip, size*sizeof(char),0);
				if (nbytes==0)
				{
					printf("Passando para modo stand alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				pthread_rwlock_wrlock(&(clip_lock[region]));
				aux_free=clip[region];
				clip[region]=aux_clip;
				pthread_rwlock_unlock(&(clip_lock[region]));
				free(aux_free);
			}
			else
			{
				printf("Unkwon traffic from socket. Change to Stand Alone.\n");
				pthread_rwlock_wrlock(&root_lock);
				close(*fd);
				root_fd=-1;
				pthread_rwlock_unlock(&root_lock);
				break;
			}
		}
		else
		{
			printf("Unkwon traffic from socket. Change to Stand Alone.\n");
			pthread_rwlock_wrlock(&root_lock);
			close(*fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			break;
		}
	}
	exit(-1);
}

void * thread_accept(void * arg)
{
	int *fd = (int*) arg;
	struct sockaddr_un client_aux_addr;
	struct sockaddr_in clip_aux_addr;
	socklen_t size_addr=0;
	int aux_fd=-1;
	pthread_attr_t attr;
	node* new_node=NULL;

	while(1)
	{
		if(*fd==in_fd)
		{
			memset(&client_aux_addr, (int) '\0', sizeof(client_aux_addr));
			size_addr=sizeof(struct sockaddr);
			aux_fd = accept(*fd, (struct sockaddr *)&client_aux_addr, &size_addr);
			if (aux_fd==-1)
				break;
			if ((new_node = add_node(aux_fd, 1))==NULL)
				close(aux_fd);
			pthread_attr_init(&attr);
			pthread_create(&(new_node->thread), &attr, thread_client, new_node);/* new_Client*/ 
		}
		else
		{
			memset(&clip_aux_addr, (int) '\0', sizeof(clip_aux_addr));
			size_addr=sizeof(struct sockaddr);
			aux_fd = accept(*fd, (struct sockaddr *)&clip_aux_addr, &size_addr);
			if (aux_fd==-1)
				break;
			if ((new_node = add_node(aux_fd, 0))==NULL)
				close(aux_fd);
			pthread_attr_init(&attr);
			pthread_create(&(new_node->thread), &attr, thread_clip, new_node); /*new_clip*/ 
		}	
	}
	/* FIXME free e close da lista */
	close(*fd);
	exit(-1);
}

node * add_node(int fd, int head)
{
	node* new_node=NULL;

	if((new_node=(node*)malloc(sizeof(node)))==NULL)
		return NULL;
	memset(new_node, (int) '\0', sizeof(node));
	new_node->fd=fd;
	if(head==0)
	{
		pthread_rwlock_wrlock(&hclip_lock);
		new_node->next=head_clip;
		head_clip=new_node;
		pthread_rwlock_unlock(&hclip_lock);
	}
	else
	{
		pthread_rwlock_wrlock(&hclient_lock);
		new_node->next=head_client;
		head_client=new_node;
		pthread_rwlock_unlock(&hclient_lock);
	}
	return new_node;
}

void remove_node(int fd, int head)
{
	node * tmp=NULL;
	node * aux_head=NULL;
	node * prev=NULL;
	int done=0;
	
	if(head==1)
	{
		pthread_rwlock_rdlock(&hclient_lock);
		aux_head=head_client;
		pthread_rwlock_unlock(&hclient_lock);
	}
	else
	{
		pthread_rwlock_rdlock(&hclip_lock);
		aux_head=head_clip;
		pthread_rwlock_unlock(&hclip_lock);
	}

	for(tmp=aux_head;(tmp!=NULL || !done );tmp=tmp->next)
	{
		if(tmp->fd==fd) /* se o no atual é o nó a remover */
		{
			done=1;
			break;
		}
		else
			prev=tmp;
	}
	if(done)
	{
		if (tmp==aux_head) /* se for o head a sair */
		{
			if(head==1)
			{
				pthread_rwlock_wrlock(&hclient_lock);
				head_client=tmp->next;
				pthread_rwlock_unlock(&hclient_lock);
			}
			else
			{
				pthread_rwlock_wrlock(&hclip_lock);
				head_clip=tmp->next;
				pthread_rwlock_unlock(&hclip_lock);
			}
		}
		else
		{
			if(head==1)
			{
				pthread_rwlock_wrlock(&hclient_lock);
				prev->next=tmp->next;
				pthread_rwlock_unlock(&hclient_lock);
			}
			else
			{
				pthread_rwlock_wrlock(&hclip_lock);
				prev->next=tmp->next;
				pthread_rwlock_unlock(&hclip_lock);
			}
		}
		free(tmp);
	}
}

void * thread_clip(void* arg)
{
	int fd  = ((node*)arg)->fd;
	char* aux_clip = NULL;
	char* aux = NULL;
	char default_messages[10];
	int i=0;
	int size=0, region=0;
	int nbytes =0 ;

	while(1)
	{
		region=99;
		size=0;
		memset(default_messages, (int) '\0', sizeof(char)*10);			
		nbytes = recv(fd, default_messages, sizeof(char)*10, 0);
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The clip has exited.\n");
			break;
		}
		else
		{
			/* REVER ESTE INICIO */
			sscanf(default_messages, "%d:%d", &region, &size);

			if(!((region<10 && region>=0) || region==OPER_CODE))
				break;

			/* receives message */
			if(region<10 && region>=0)
			{
				if(aux_clip!=NULL)
					free(aux_clip);
				if((aux_clip=(char*)malloc((size+1)*sizeof(char)))==NULL)
					break;
				memset(aux_clip, (int) '\0', (size+1)*sizeof(char));
				nbytes = recv(fd, aux_clip, size*sizeof(char), 0);
				if (nbytes==0)
				{
					printf("Didnt receive a thing. Removing clip.\n");
					break;
				}
				else
				{
					pthread_rwlock_rdlock(&root_lock);
					if(root_fd!=-1)
					{
						pthread_rwlock_unlock(&root_lock);
						send_root(aux_clip, size, region);
					}
					else
					{
						pthread_rwlock_unlock(&root_lock);
						/* actualiza regiao */
						pthread_rwlock_wrlock(&(clip_lock[region]));
						aux=clip[region];
						clip[region]=aux_clip;
						pthread_rwlock_unlock(&(clip_lock[region]));
						free(aux);
						/* envia aos clips */
						send_clips(aux_clip, size, region);
					}
				} 
			}
			else if(region==OPER_CODE)
			{
				if(size==FETCH_CODE)
				{
					for(i=0;i<10;i++)
					{
						memset(default_messages, (int)'\0', sizeof(char)*10);
						pthread_rwlock_rdlock(&(clip_lock[i]));
						if(clip[i]!=NULL)
							sprintf(default_messages, "%lu", strlen(clip[i]));
						else
							sprintf(default_messages, "%d", 0);
						nbytes = send(fd, default_messages, sizeof(char)*10, 0);
						if (nbytes<=0)
						{
							pthread_rwlock_unlock(&(clip_lock[i]));	
							break;
						}
						if(clip[i]!=NULL)
						{
							nbytes = send(fd, clip[i], sizeof(char)*strlen(clip[i]), 0);
							if (nbytes<=0)
							{
								pthread_rwlock_unlock(&(clip_lock[i]));	
								break;
							}
						}
						pthread_rwlock_unlock(&(clip_lock[i]));	
					}
				}
				else
					break;
			}
			else /* action À toa */
			{
				printf("Unable to proceed. Moving on.\n");
				break;
			}
		}
	}
	remove_node(fd, 0);
	close(fd);
	pthread_exit(NULL);
}

void * thread_client(void* arg)
{
	int fd  = ((node*)arg)->fd;
	char* aux_clip = NULL;
	char* aux = NULL;
	char default_messages[10];
	char action='\0';
	int size=0, region=0;
	int nbytes =0 ;

	while(1)
	{
		action='\0';
		region=100;
		size=0;
		memset(default_messages, (int) '\0', sizeof(char)*10);			
		nbytes = recv(fd, default_messages, sizeof(char)*10, 0);
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The client has exited.\n");
			break;
		}
		else
		{
			sscanf(default_messages, "%c:%d:%d", &action, &region, &size);
			if(action=='c') /* when received a copy order */
			{
				if(aux_clip!=NULL)
					free(aux_clip);

				if((aux_clip=(char*)malloc((size+1)*sizeof(char)))==NULL)
				{
					printf("Error malloc message. Unable to accept changes."); 
					memset(default_messages, (int) '\0', sizeof(char)*10);
					sprintf(default_messages, "ER"); /* in case of error returns ER */
					nbytes = send(fd, default_messages, sizeof(char)*10, 0);
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message\n");
						break;
					}
					continue;
				}
				memset(default_messages, (int) '\0', sizeof(char)*10);
				sprintf(default_messages, "OK"); /* ready para receber a mensagem */
				nbytes = send(fd, default_messages, sizeof(char)*10, 0);
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Unable to send message. Removing client.\n");
					break;
				}
				/* receives message */
				memset(aux_clip, (int) '\0', (size+1)*sizeof(char));
				nbytes = recv(fd, aux_clip, size*sizeof(char), 0);
				if (nbytes==0)
				{
					printf("Didnt receive a thing. Removing client.\n");
					break;
				}
				else
				{

					pthread_rwlock_rdlock(&root_lock);
					if(root_fd!=-1)
					{
						pthread_rwlock_unlock(&root_lock);
						send_root(aux_clip, size, region);
					}
					else
					{
						pthread_rwlock_unlock(&root_lock);
						pthread_rwlock_wrlock(&(clip_lock[region]));
						aux=clip[region];
						clip[region]=aux_clip;
						pthread_rwlock_unlock(&(clip_lock[region]));
						free(aux);
						send_clips(aux_clip, size, region);
					}
				} 
			}
			else if(action=='p')
			{
				pthread_rwlock_rdlock(&(clip_lock[region]));
				aux_clip=clip[region];
				pthread_rwlock_unlock(&(clip_lock[region]));
				memset(default_messages, (int) '\0', sizeof(char)*10);
				if(aux_clip==NULL) 
				{
					sprintf(default_messages, "%d", 0);
				    nbytes=send(fd, default_messages, sizeof(char)*10, 0); /* envia só um 0 */
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message. Removing client.\n");
						break;
					}
				}
				else
				{
					if(strlen(aux_clip)>(size))
					{
						nbytes = sprintf(default_messages, "%d", size);
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*(size),0);
						if (nbytes<=0) /*(nbytes<size) */
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
					else
					{
						nbytes = sprintf(default_messages, "%lu", strlen(aux_clip));
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*strlen(aux_clip),0);
						if (nbytes<=0) /* (nbytes<strlen(aux_clip)) */
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
				}
			}
			else if(action=='w')
			{
				pthread_rwlock_rdlock(&(clip_lock[region]));
				aux_clip=clip[region];
				pthread_rwlock_unlock(&(clip_lock[region]));
				memset(default_messages, (int) '\0', sizeof(char)*10);
				if(aux_clip==NULL) 
				{
					sprintf(default_messages, "%d", 0);
				    nbytes=send(fd, default_messages, sizeof(char)*10, 0); /* envia só um 0 */
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message. Removing client.\n");
						break;
					}
				}
				else
				{
					if(strlen(aux_clip)>(size))
					{
						nbytes = sprintf(default_messages, "%d", size);
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*(size),0);
						if (nbytes<=0) /*(nbytes<size)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
					else
					{
						nbytes = sprintf(default_messages, "%lu", strlen(aux_clip));
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*strlen(aux_clip),0);
						if (nbytes<=0) /*(nbytes<strlen(aux_clip))*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
				}
			}
			else if(action=='q')
			{
				/* quer dizer que este cliente vai sair -> há que fechar ligação */
				memset(default_messages, (int) '\0', sizeof(char)*10);
				sprintf(default_messages, "OK");
				nbytes = send(fd, default_messages, sizeof(char)*10, 0);
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Unable to send message. Removing client.\n");
					break;
				}
				remove_node(fd, 1);
				close(fd);
				pthread_exit(NULL);
			}
			else /* action À toa */
				printf("Unable to proceed. Moving on.\n");
		}
	}
	close(fd);
	remove_node(fd, 0);
	pthread_exit(NULL);
}

void send_root(char* new, int size, int region)
{
	int nbytes=0;
	char default_messages[10];
	char * aux=NULL;

	memset(default_messages, (int) '\0', 10*sizeof(char));
	sprintf(default_messages, "%d:%d", region, size);
	/* root avisado */
	nbytes = send(root_fd, default_messages, sizeof(char)*10, 0);
	if (nbytes<=0) /*(nbytes<10)*/
	{	
		printf("Root exited. Becoming root.\n");
		pthread_rwlock_rdlock(&root_lock);
		if(root_fd!=-1)
		{	
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&root_lock);
			close(root_fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&(clip_lock[region]));
			aux=clip[region];
			clip[region]=new;
			pthread_rwlock_unlock(&(clip_lock[region]));
			free(aux);
			send_clips(new, size, region);
		}
		else
			pthread_rwlock_unlock(&root_lock);
	}
	/* send the message */
	nbytes = send(root_fd, new, sizeof(char)*size, 0);
	if (nbytes==0)
	{
		printf("Root exited. Becoming root.\n");
		pthread_rwlock_rdlock(&root_lock);
		if(root_fd!=-1)
		{
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&root_lock);
			close(root_fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&(clip_lock[region]));
			aux=clip[region];
			clip[region]=new;
			pthread_rwlock_unlock(&(clip_lock[region]));
			free(aux);
			send_clips(new, size, region);
		}
		else
			pthread_rwlock_unlock(&root_lock);
	}
}

void send_clips(char* new, int size, int region)
{
	node* tmp=NULL;
	node* head=NULL;
	int nbytes=0;
	char default_messages[10];

	pthread_rwlock_rdlock(&hclip_lock);
	head = head_clip;
	pthread_rwlock_unlock(&hclip_lock);

	memset(default_messages, (int)'\0', sizeof(char)*10);
	sprintf(default_messages, "%d:%d", region, size);
	for(tmp=head;tmp!=NULL;tmp=tmp->next)
	{
		nbytes = send(tmp->fd, default_messages, sizeof(char)*10, 0);
		if (nbytes==0)
		{
			close(tmp->fd);
			remove_node(tmp->fd, 0);
		}
		nbytes = send(tmp->fd, new, sizeof(char)*size, 0);
		if (nbytes==0)
		{
			close(tmp->fd);
			remove_node(tmp->fd, 0);
		}
	}
}