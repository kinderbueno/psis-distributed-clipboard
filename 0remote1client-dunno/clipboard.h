#define INBOUND_FIFO "INBOUND_FIFO"
#define OUTBOUND_FIFO "OUTBOUND_FIFO"
#define MAXLEN 1000000
#define STDLEN 100 
#define SOCK_ADDRESS "/tmp/sock_16"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>

typedef struct Request
{
	char action;
	int region;
	int size;
} request;

typedef struct Client
{
	int fd;
	struct sockaddr_un addr;
	socklen_t size;
	struct Client* next;
} client;

int clipboard_connect(char * clipboard_dir);
int clipboard_copy(int clipboard_id, int region, void *buf, size_t count);
int clipboard_paste(int clipboard_id, int region, void *buf, size_t count);
void clipboard_close(int clipboard_id);

