#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <unistd.h>
#include <signal.h>

#include "sock_stream.h"

char socket_name[100];

void ctrl_c_callback_handler(int signum){
	printf("Caught signal Ctr-C\n");
	unlink(SOCK_ADDRESS);
	exit(0);
}
	



int main(){
	struct sockaddr_un local_addr;
	struct sockaddr_un client_addr;
	 socklen_t size_addr;

	char buff[100];
	int nbytes;
	
	signal(SIGINT, ctrl_c_callback_handler);
	
	int sock_fd= socket(AF_UNIX, SOCK_STREAM, 0);
	
	if (sock_fd == -1){
		perror("socket: ");
		exit(-1);
	}
	

	local_addr.sun_family = AF_UNIX;
	strcpy(local_addr.sun_path, SOCK_ADDRESS);

	

	int err = bind(sock_fd, (struct sockaddr *)&local_addr, sizeof(local_addr));
	if(err == -1) {
		perror("bind");
		exit(-1);
	}
	printf(" socket created and binded \n");
	
	if(listen(sock_fd, 2) == -1) {
		perror("listen");
		exit(-1);
	}


	while(1){
		
		printf("%d Ready to accept connections\n", getpid());

		int client_fd= accept(sock_fd, (struct sockaddr *) & client_addr, &size_addr);
		if(client_fd == -1) {
			perror("accept");
			exit(-1);
		}
		if(fork()==0){
			printf("acceped connection from %s\n", client_addr.sun_path);
			int err_rcv;
			int len_message;
			while((err_rcv = recv(client_fd, buff, 100, 0)) >0 ){
				printf("%d %d - %s\n", getpid(), err_rcv, buff);
				len_message = strlen(buff);
				write(client_fd, &len_message, sizeof(len_message));
			}
			exit(0);
		}
	}
	exit(0);
}
