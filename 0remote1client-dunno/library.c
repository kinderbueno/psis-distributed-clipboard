#include "clipboard.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct sockaddr_un clip_addr;
request* req=NULL;


int clipboard_connect(char * clipboard_dir)
{
	int err=0, 
	fd=0;

	fd=socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("socket: ");
		return -1;
	}

	memset(&clip_addr,0,sizeof(clip_addr));
	clip_addr.sun_family = AF_UNIX;
	strcpy(clip_addr.sun_path, clipboard_dir);

	err=connect(fd, (struct sockaddr *) &clip_addr, sizeof(clip_addr));
	if(err == -1)
	{
		perror("Error connecting\n"); /* FIXME: isto tem perror ? */
		return -1;
	}

	if ((req=(request*)malloc(sizeof(request)))==NULL)
	{
		printf("Unable to malloc request");
		return -1;
	}
	memset(req,0,sizeof(request));

	return fd;
}

/* devia haver 3 saidas: 1 correu tudo bem, 0 não aconteceu nada do outro lado, -1 deu erro e o clip morreu */
int clipboard_copy(int clipboard_id, int region, void *buf, size_t count)
{
	int nbytes=0;
	char msg[10];

	*msg='\0'; /* reset da mensagem */

	req->region=region;
	req->size=count;
	req->action='c';

	/* FIXME: verificar todos os sends */
	/* FIXME: nbyts<0 ? */
	nbytes = send(clipboard_id, req, sizeof(request), 0); /* envia request */
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recebe 1º OK ?*/
	if (nbytes==0)
	{
		printf("Error receiving 1st OK. No chages were made. (Clipboard quited unexpectedly)\n");
		/* shutdown */
		return 0;
	}
	else
		*(msg+2)='\0';
	printf("msg=%s\n", msg);
	if(!strcmp(msg,"OK")) /* 1º OK recebido */
	{
		nbytes=0;
		*msg='\0'; /* reset da mensagem */
		nbytes = send(clipboard_id, (char*) buf, sizeof(char)*count, 0); /* envia texto */
		nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recebe 2º OK ? */
		if (nbytes<=0)
		{
			printf("Error receiving 2º OK. No changes were made. (clipbord quited unexpectedly)\n");
			/* shutdown */
			return 0;
		}
		else
			*(msg+2)='\0';
		if(!strcmp(msg,"OK")) /* 2º OK recebido */
			return 1;
		else if(!strcmp(msg,"ER"))
		{
			printf("Error alocating memory. Probably its too big. Clipboard shuteddown.\n");
			/* shutdown */
			return 0;
		}
		else
		{
			printf("Error 2nd OK. Received - %s - instead.\n", msg);
			/* fazer o que ? isto é sequer válido ? */
			return 0;
		}	
	}
	else if(!strcmp(msg,"ER"))
	{
		printf("Error. Bad request probably ?\n");
		/* fazer o que ? isto é sequer válido ? */
		return 0;
	}
	else
	{
		printf("Error 1st OK. Received - %s - instead.\n", msg);
		return 0;
	}

}

/* FIXME: tres resultados: 1 correu bem, 0 nao havia nada, -1 deu erro (fazer exit forçado) */
int clipboard_paste(int clipboard_id, int region, void *buf, size_t count) /* count vai ter de ser a memoria alocada para o buf */
{	
	int nbytes=0, size=0;
	char msg[10];

	*msg='\0'; /* reset da mensagem */

	req->region=region;
	req->size=0;
	req->action='p';
	
	/* FIXME: não por prints aqui e por na app */
	nbytes = send(clipboard_id, req, sizeof(request), 0);
	printf("sent request\n");
	nbytes = recv(clipboard_id, &size, sizeof(int), 0);
	printf("received size: %d\n", size);
	if(nbytes==0)
	{
		printf("Nothing was received... Clipboard exited unexpectedly\n");
		/* shutdown */
		return 0;
	}
	if(!strcmp(msg,"ER"))
	{
		printf("Error. Bad request probably ?\n");
		return 0;
	}
	else if(size!=0) /* aqui assume-se que se recebeu um OK */
	{
		/* se o return for o size da alocada na msg dá para por aqui if size>count */
		

		/* free(buf);
		if((buf=(char*)malloc(size*sizeof(char)))==NULL)
		{
			printf("Error malloc. Aborting.\n");
			sprintf(msg, "ER");
			nbytes = send(clipboard_id, msg, sizeof(char)*strlen(msg), 0);
			//shutdown , será que se pode evitar este ? 
			return 0; 
		} */

		sprintf(msg, "OK");
		nbytes = send(clipboard_id, msg, sizeof(char)*strlen(msg), 0);
		printf("sent ok.\n");

		nbytes = recv(clipboard_id, buf, sizeof(char)*size, 0); /* recepçao da mensagem */
		printf("received message\n");
		if(nbytes==0)
		{
			printf("Nothing was received... Clipboard exited unexpectedly.\n");
			/* shutdown */
			return 0;
		}
		else
		{
			*((char*)buf+size)='\0'; /* verificar isto */
			/* sprintf(msg, "OK");
			nbytes = send(clipboard_id, msg, sizeof(char)*strlen(msg), 0); */
			printf("done\n");
			return 1;
		}
	}
	else
	{
		printf("Nothing on the region!\n");
		return 1;
	}
}

void clipboard_close(int clipboard_id)
{
	char msg[10];
	int nbytes=0;

	*msg='\0'; /* reset da mensagem */

	req->action='q'; /* avisa que vai sair */

	/* FIXME: verificar este send e o receive */
	nbytes = send(clipboard_id, req, sizeof(request), 0);
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* deverá receber um OK (FIXME: nao ficar preso aqui se por alguma o clip vai abaixo) */
	if(nbytes==0)
	{
		printf("Nothing was received... Clipboard exited unexpectedly.\n");
		/* shutdown */
	}
	else
		*(msg+2)='\0'; 
	if(!strcmp(msg,"ER"))
	{
		printf("Error. Bad request probably ?\n Closing the program anyway.\n"); /* vale a pena enviar outra vez ? */
	}
	else if(strcmp(msg,"OK")) /* se nao for OK */
		printf("error, expecting an OK and received: %s \nClosing the program anyway.\n", msg);

	close(clipboard_id);
	free(req);
}