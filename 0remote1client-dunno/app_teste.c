#include "clipboard.h"

#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
		int fd=-1;
		char input[10], act='\0', msg[MAXLEN];
		int dados_int=0, reg, size=0;

		/* connects to the clipboard */
		fd = clipboard_connect(SOCK_ADDRESS); 

		while(1)
		{
			/* define action :  copy, paste or quit */
			act='m';
			while(act!='c' && act!='C' && act!='p' && act!='P' && act!='q' && act!='Q')
			{
				printf("Action: copy or paste? [c/p]\n(or type 'Q' to quit the program)\n");
				fgets(input, 10, stdin); /* FIXME: algum getchar especifico ? */
				act=input[0];
			}

			reg=20;
			/* when not quiting */
			if (act!='Q' && act!='q')
			{
				/* checks the region */
				while(reg<-1 || reg>10)
				{
					printf("Region? [1-10]\n");
					fgets(input, 10, stdin);
					reg=atoi(input); /* FIXME: verificar o número como deve ser*/ 
					reg--; /* para ficar de 0 a 9 */
				}

				/* if copy */
				if(act=='c' || act=='C')
				{
					printf("Text?\n");
					fgets(msg, MAXLEN, stdin); /* FIXME: arranjar forma de tirar este MAXLEN daqui (ISSUE)*/
					size=strlen(msg); /* SIZE CONTEM O BARRA 0*/
					printf("ola...\n");
					if(!clipboard_copy(fd, reg, msg, size))
					{
						printf("Error copying to the clipboard. Aborting.");
						exit(-1);
					}
					printf("Sucessfully copied!\n");
					/*printf("DADOS:\n\t* regiao - %d;\n\t* acao - %c;\n\t* mensagem - %s\t* size - %d\n", reg, act, msg, size);*/
				}
				/* if paste */
				else if(act=='p' || act!='P')
				{
					size=MAXLEN;
					/* infos sent is the fd and reg and size is the malloc of the msg. The msg will be filled with the paste (and reallocated if needed) */
					if(!clipboard_paste(fd, reg, msg, size)) 
					{
						printf("Error pasting to clipboard. Aborting.");
						exit(-1);
					}
					printf("Received:\n %s\n -End Of Message\n", msg);
				}
				else /* no copy or paste */
					printf("Unrecognized command. Please try again.\n");
			}
			else /* if quit */
			{
				clipboard_close(fd);
				/* free msg ? */
				break;
			}
		}	
		printf("Cya! :)\n");
		exit(0);
	}
