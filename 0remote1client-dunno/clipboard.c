#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "clipboard.h"

 
int main()
{
	int local_fd=0; /* for new clients*/
	int nclients=0; /* number os clients of the clipboard */ /* ver se isto e mm necessario */
	struct sockaddr_un local_addr; /* for new connections */ 
	char default_messages[10]; /* OK(ACK) and ERROR messages */
	client* new_client; /* head of the clients list */
	char* clip[10];	/* clip itself */
	int size[10]; /* size of the allocated memory of the clip (diferent from strlen) */

	/* auxiliar variables */
	int auxfd=0;
	request *req=NULL;
	socklen_t size_addr; 
	struct sockaddr_un aux_addr; /* for new connections */ 
	client* next_client=NULL;
	int err=0, i=0, nbytes=0, stop=0;


	/* criar socket e defini-lo na estrutura e por o programa à escuta */
	local_fd= socket(AF_UNIX, SOCK_STREAM, 0);
	if (local_fd == -1){
		perror("socket: ");
		exit(-1);
	}

	/* mmset de coisas !! addrs e outros */
	memset(&local_addr,0,sizeof(local_addr));
	local_addr.sun_family = AF_UNIX;
	strcpy(local_addr.sun_path, SOCK_ADDRESS);
	unlink(SOCK_ADDRESS); /* para evitar erros de bind quando o programa vai abaixo */

	err = bind(local_fd, (struct sockaddr *)&local_addr, sizeof(local_addr));
	if(err == -1){
		perror("bind");
		exit(-1);
	}

	/* FIXME: onde é que isto fica ? */
	listen(local_fd, 5);

	/* por o size a zeros -> referente à memoria alocada e não ao tamanho real da palavra (poderá ser menor) */
	for(i=0;i<10;i++)
	{
		size[i]=0;
		clip[i]=NULL;
	}

	new_client=NULL;

	 /* criar estrutura de request -> só é preciso fazer uma vez! */
	if ((req=(request*)malloc(sizeof(request)))==NULL)
	{
		printf("Error malloc request. Aborting.");
		exit(-1);
	}

	memset(req,0,sizeof(request));

		/* fica a espera que alguém se ligue ao socket e  adiciona se à lista de clientes */
	auxfd = accept(local_fd, (struct sockaddr *)&aux_addr, &size_addr);

	/* adicionar o cliente à lista */
	next_client=new_client;
	if ((new_client=(client*)malloc(sizeof(client)))==NULL)
	{
		printf("Error malloc client. Aborting.");
		exit(-1);
	}
	memset(new_client,0,sizeof(*new_client));
	new_client->fd=auxfd;
	new_client->addr=aux_addr;
	new_client->size=size_addr;
	new_client->next=next_client; /* o novo cliente fica então a cabeça da lista */
	nclients++;

	printf("connected\n");

	while(1)
	{
		/* POR AQUI UM SELECT . Enfiar as coisas de threads depois ?  adicionar uma opçao de quit ? e talvez show do clip mais o show que o prof pede no enunciado*/
		
		if (nclients==0) break;
		/* recebe infos */
		nbytes = recv(new_client->fd, req, sizeof(request), 0); /* função aqui ! */
		if (nbytes==0)
		{
			printf("didnt receive a thing. -> The client has exited unexpectadly.\n");
			/* remove client */
			nclients--;
			stop=1;
		}
		else
		{
			printf("received smth\n");
			if(req->action=='c') /* when received a copy order */
			{
				if(size[req->region]<req->size) /* realloc if needed */ 
				{
					if (size[req->region]!=0)
					{
						free(clip[req->region]);
					} 
					else
						printf("didnt free\n");

					if((clip[req->region]=(char*)malloc(req->size*sizeof(char)))==NULL)
					{
						printf("error malloc message"); /* FIXME: perror ? Será que dar para nao mandar abaixo caso a mensagem seja demasiado grande ou assim ?*/
						sprintf(default_messages, "ER"); /* in case of error returns ER */
						nbytes = send(new_client->fd, default_messages, sizeof(char)*strlen(default_messages), 0);
						printf("sent error\n");
						exit(-1);
					}
					size[req->region]=req->size;
				}

				sprintf(default_messages, "OK"); /* ready para receber a mensagem */
				nbytes = send(new_client->fd, default_messages, sizeof(char)*strlen(default_messages), 0);
				printf("sent ok->%s\n", default_messages);
				/* receives message */
				nbytes = recv(new_client->fd, clip[req->region], req->size*sizeof(char), 0);
				if (nbytes==0)
				{
					printf("didnt receive a thing...\n");
					/* remove client */
					nclients--;
				}
				else
				{
					printf("received: %s\n", clip[req->region]);
					sprintf(default_messages, "OK");
					nbytes = send(new_client->fd, default_messages, sizeof(char)*strlen(default_messages), 0);
					printf("ok sent\n");
				}

				/* difundir a noticia aqui para fora daqui*/
				
			}
			else if(req->action=='p')
			{
				if(size[req->region]==0 || strlen(clip[req->region])==1) 
				{
					i=0;
					/* envia uma zero de volta porque a posição de memória não tem nada, 1 pq strten contem barra 0*/
				    nbytes = send(new_client->fd, &i, sizeof(int), 0); 
				}
				else
				{
					i=strlen(clip[req->region]);
					nbytes = send(new_client->fd, &i , sizeof(int), 0);
					nbytes = recv(new_client->fd, default_messages, 10*sizeof(char), 0);
					if (nbytes==0)
					{
						printf("didnt receive a thing...\n");
						/* remover cliente */
						nclients--;
					}
					else if (!strcmp(default_messages, "OK"))
					{
						nbytes = send(new_client->fd, clip[req->region], strlen(clip[req->region])*sizeof(char), 0);
						printf("Message sent.\n"); 
					}
					else if (!strcmp(default_messages, "ER"))
					{
						printf("Client doesnt have space. Not sending. \n");
						/* remove cliente + fecha a ligação */
						nclients--;
					}
					else /* vale a pena por isto ? */
						printf("Smth happened. Expected un OK/ER and received - %s - instead.\n", default_messages );
				}
			}
			else if(req->action=='q')
			{
				/* quer dizer que este cliente vai sair -> há que fechar ligação */
				/* FIXME: procurar o cliente na lista e elimina-lo, depois enviar um OK, depois ha que o tirar do select (se calhar faz se isso automaticamente)*/
				sprintf(default_messages, "OK");
				nbytes = send(new_client->fd, default_messages, sizeof(char)*strlen(default_messages), 0);
				printf("sent ok\n");
				close(new_client->fd);
				/* free(new_client); */
				nclients--;
				break; 
			}
			else
			{
				sprintf(default_messages, "ER");
				nbytes = send(new_client->fd, default_messages, sizeof(char)*strlen(default_messages), 0);
				printf("sent error!!!\n");
			}

		}

		if(!stop)
		{
			printf("\n");
			printf("nclients=%d\n", nclients);
			printf("CLIPBOARD: (reg:malloc:strlen -> string)\n");
			for(i=0;i<10;i++)
			{
				if (size[i]!=0)
					printf("%d:%d->%s", i+1, size[i], clip[i]);
				else 
					printf("%d:%d->\n", i+1, size[i]);
			}
			printf("\n");
		}

		
	}
	/* eliminar a estrutura do request*/
	free(req);

	/* free das estruturas abertas quando exit */
	for(i=0;i<10;i++)
		if(size[i]!=0)
			free(clip[i]);

	/* free do client. FIXME: depois vai ter de iterar por todos os clientes */ 
	free(new_client);
		
	exit(0);
	
}
