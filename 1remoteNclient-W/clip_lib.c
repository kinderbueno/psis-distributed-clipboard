#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "clip_lib.h"
#include "ext_var.h"

/* external variables */
char * clip[10];
int root_fd;
int in_fd;
int out_fd;
node * head_clip;
node * head_clients;

pthread_rwlock_t clip_lock[10];
pthread_rwlock_t hclip_lock; 
pthread_rwlock_t hclient_lock;
pthread_rwlock_t root_lock; 

int check_args(int c, char* v[])
{
	char ip[30];
	int err=0, port=0;
	struct sockaddr_in root_addr;	

	memset(&root_addr, (int) '\0', sizeof(root_addr));
	memset(ip, (int) '\0', sizeof(char)*30);
	root_fd=-1;

	if (c==1)
		return 0; /* clipboard is alone */
	else if (c==4)
	{
		/* scans through the arguments and verifies them before adding to the stucture 	*/
		if(!strcmp("-c", v[1]))
		{
			if(sscanf(v[2], "%s", ip)!=1 || !inet_aton(ip, &(root_addr.sin_addr)))
			{
				printf("Invalid given IP. Not connecting\n");
				return -1;
			}
			if(sscanf(v[3], "%d", &port)!=1 || !(port>=MINPORT && port<=MAXPORT) )
			{
				printf("Invalid given port. Not connecting. It should be a integer between %d and %d.\n", MINPORT, MAXPORT);
				return -1;
			}

			/* arguments are now verified  */
			root_addr.sin_family = AF_INET;
			root_addr.sin_port = htons((u_short) port);
			/* central_server->sin_addr.s_addr = server_ip.s_addr; ---> isto ja esta feito acima */

			root_fd = socket(AF_INET, SOCK_STREAM, 0);
			if (root_fd == -1){
				perror("socket: ");
				return -1;
			}

			err = connect(root_fd,(struct sockaddr *)&root_addr, sizeof(root_addr));
			if(err==-1)
			{
				printf("Error connecting to next clipboard. Not connecting.\n");
				close(root_fd);
				return -1;
			}
			return root_fd; 
		}
		else 
		{
			printf("Invalid arguments. Not connecting. Use the following:\n./clipboard [-c ip_next port_next]\n");
			return -1;
		}	
	}
	else
	{
		printf("Invalid arguments. Not connecting. Use the following:\n./clipboard [-c ip_next port_next]\n");
		return -1;
	}
}

int fetch_clip(int fd)
{
	char default_messages[10];
	char msg[MAXLEN];
	char* aux_clip[10];
	int aux_size[10];
	int i=0, j=0, nbytes=0;

	printf("shit1\n");
	
	memset(default_messages, (int) '\0', sizeof(char)*10);
	memset(msg, (int) '\0', sizeof(char)*MAXLEN);
	/* send fetch request */
	sprintf(default_messages, "%d:%d", OPER_CODE, FETCH_CODE);
	nbytes = send(fd, default_messages, sizeof(char)*10, 0);
	if (nbytes<=0) /* (nbytes<10)*/
	{
		printf("Unable to send message.\n");
		return 0;
	}
	printf("message sent\n");

	/* recieve sizes */
	nbytes = recv(fd, msg, sizeof(char)*MAXLEN, 0); /* n de bytes */
	if (nbytes==0)
	{
		printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
		return 0;
	}
	else
	{
		printf("message received: %s\n", msg);
		sscanf(msg, "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d", 
				&(aux_size[0]), &(aux_size[1]), &(aux_size[2]), &(aux_size[3]), &(aux_size[4]),
				&(aux_size[5]), &(aux_size[6]), &(aux_size[7]), &(aux_size[8]), &(aux_size[9]));
		printf("%d:%d:%d:%d:%d:%d:%d:%d:%d:%d", 
				aux_size[0], aux_size[1], aux_size[2], aux_size[3], aux_size[4],
				aux_size[5], aux_size[6], aux_size[7], aux_size[8], aux_size[9]);
	}

	/* try to allocate all memory */
	printf("CENAS\n");
	for(i=0; i<10; i++)
	{
		printf("voltinhas\n");
		aux_clip[i]=NULL;
		if((aux_clip[i]=(char*)malloc(sizeof(char)*aux_size[i]))==NULL)
		{
			printf("Error malloc.\n");
			memset(default_messages, (int) '\0', sizeof(char)*10);
			sprintf(default_messages, "ER");
			nbytes = send(fd, default_messages, sizeof(char)*10, 0);
			if (nbytes<=0) /*(nbytes<10)*/
			{
				printf("Unable to send message.\n");
				return 0;
			}
			for(j=0;j<i;j++)
				free(aux_clip[j]);
			return 0;
		}
	}
	printf("Hurray\n");
	memset(default_messages, (int) '\0', sizeof(char)*10);
	sprintf(default_messages, "OK");
	nbytes = send(fd, default_messages, sizeof(char)*10, 0);
	if (nbytes<=0) /*(nbytes<10)*/
	{
		printf("Unable to send message.\n");
		return 0;
	}
	printf("message sent... %s\n", default_messages);

	/* receber as palavras */
	for (i=0;i<10;i++)
	{
		memset(aux_clip[i], (int) '\0', sizeof(char)*aux_size[i]);
		nbytes = recv(fd, aux_clip[i] , sizeof(char)*aux_size[i], 0); /* n de bytes */
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
			for(j=0;j<10;j++)
				free(aux_clip[j]);
			return 0;
		}
	}

	/* se correu tudo bem: precaução para não ficarem coisas a meio */
	for (i=0;i<10;i++)
		clip[i]=aux_clip[i];

	return 1;
}

void * thread_root(void * arg)
{
	int * fd = (int *) arg;
	int nbytes=0, region=0, size=0;
	char* aux_clip=NULL, *aux_free=NULL;
	char default_messages[10];

	printf("ROOT THREAD YEAH\n");
	printf("root fd = %d\n", root_fd);
	/* ja tenho os clips sincronizados */
	while(1)
	{
		printf("Root thread waiting...\n");
		nbytes=recv(*fd, default_messages, 10*sizeof(char),0);
		if (nbytes==0)
		{
			printf("Passando para modo stand alone.\n");
			pthread_rwlock_wrlock(&root_lock);
			close(*fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			break;
		}
		printf("Root thread received smth !!%s!!\n", default_messages);
		sscanf(default_messages, "%d:%d", &region, &size);
		if(region<10 || region>=0)
		{
			if (size>0)
			{
				/* este malloc esta protegido para remover a ligaçao com o Clipboard acima pois fica num estado inconsistente */
				if((aux_clip=(char*)malloc(sizeof(char)*(size+1)))==NULL) 
				{
					printf("Malloc Error. Change to Stand Alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Passando para modo stand alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				memset(aux_clip, (int) '\0', (size+1)*sizeof(char));
				nbytes=recv(*fd, aux_clip, size*sizeof(char),0);
				if (nbytes==0)
				{
					printf("Passando para modo stand alone.\n");
					pthread_rwlock_wrlock(&root_lock);
					close(*fd);
					root_fd=-1;
					pthread_rwlock_unlock(&root_lock);
					break;
				}
				pthread_rwlock_wrlock(&(clip_lock[region]));
				aux_free=clip[region];
				clip[region]=aux_clip;
				pthread_rwlock_unlock(&(clip_lock[region]));
				free(aux_free);
			}
			else
			{
				printf("Unkwon traffic from socket. Change to Stand Alone.\n");
				pthread_rwlock_wrlock(&root_lock);
				close(*fd);
				root_fd=-1;
				pthread_rwlock_unlock(&root_lock);
				break;
			}
		}
		else
		{
			printf("Unkwon traffic from socket. Change to Stand Alone.\n");
			pthread_rwlock_wrlock(&root_lock);
			close(*fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			break;
		}
	}
	exit(-1);
}

void * thread_accept(void * arg)
{
	int *fd = (int*) arg;
	struct sockaddr_un client_aux_addr;
	socklen_t size_addr=0;
	int aux_fd=-1;
	pthread_attr_t attr;
	node* new_node=NULL;

	printf("thread accept ON!!\n");
	printf("accepted thread = %d; in_fd = %d; out_fd = %d.\n", *fd, in_fd, out_fd);
	while(1)
	{
		printf("thread accept waiting....\n");
		memset(&client_aux_addr, (int) '\0', sizeof(client_aux_addr));
		size_addr=sizeof(struct sockaddr); 
		/* bloqueia até alguém cliente tente fazer connect */
		aux_fd = accept(*fd, (struct sockaddr *)&client_aux_addr, &size_addr);
		if (aux_fd==-1)
			break;
		printf("NEW CONNECTION\n");
		/* FIXME ver que thread é para saber para que lista o no deve ser adicionado: 0 = clip, 1 = cliente */
		if(*fd==in_fd)
		{
			if ((new_node = add_node(aux_fd, 1))==NULL)
				close(aux_fd);
			pthread_attr_init(&attr);
			pthread_create(&(new_node->thread), &attr, thread_client, new_node);/* new_Client*/
		}
		else
		{
			if ((new_node = add_node(aux_fd, 0))==NULL)
				close(aux_fd);

			pthread_attr_init(&attr);
			/* pthread_create(&(new_node->thread), &attr, thread_client, &aux_fd); *//*new_clip*/
		}
	}
	/* FIXME free e close da lista */
	close(*fd);
	exit(-1);
}
node * add_node(int fd, int head)
{
	node* new_node=NULL;

	if((new_node=(node*)malloc(sizeof(node)))==NULL)
		return NULL;
	memset(new_node, (int) '\0', sizeof(node));
	new_node->fd=fd;
	if(head==0)
	{
		pthread_rwlock_wrlock(&hclip_lock);
		new_node->next=head_clip;
		head_clip=new_node;
		pthread_rwlock_unlock(&hclip_lock);
	}
	else
	{
		pthread_rwlock_wrlock(&hclient_lock);
		new_node->next=head_client;
		head_client=new_node;
		pthread_rwlock_unlock(&hclient_lock);
	}
	return new_node;
}

void remove_node(int fd, int head)
{
	node * tmp=NULL;
	node * aux_head=NULL;
	node * prev=NULL;
	int done=0;
	
	printf("removing node!!\n");
	if(head==1)
	{
		printf("1\n");
		pthread_rwlock_rdlock(&hclient_lock);
		aux_head=head_client;
		pthread_rwlock_unlock(&hclient_lock);
	}
	else
	{
		printf("2\n");
		pthread_rwlock_rdlock(&hclip_lock);
		aux_head=head_clip;
		pthread_rwlock_unlock(&hclip_lock);
	}

	for(tmp=aux_head;(tmp!=NULL || !done );tmp=tmp->next)
	{
		printf("voltinhas\n");
		if(tmp->fd==fd) /* se o no atual é o nó a remover */
		{
			printf("Oi!\n");
			printf("Incresing done !!\n");
			done=1;
			break;
		}
		else
		{
			printf("else!!\n");
			prev=tmp;
		}
	}
	printf("uff\n");
	if(done)
	{
		printf("ici sff\n");
		if (tmp==aux_head) /* se for o head a sair */
		{
			if(head==1)
			{
				printf("whatxa doing\n");
				pthread_rwlock_wrlock(&hclient_lock);
				head_client=tmp->next;
				pthread_rwlock_unlock(&hclient_lock);
			}
			else
			{
				printf("sai sai sai\n");
				pthread_rwlock_wrlock(&hclip_lock);
				head_clip=tmp->next;
				pthread_rwlock_unlock(&hclip_lock);
			}
		}
		else
		{
			if(head==1)
			{
				printf("yoloo\n");
				pthread_rwlock_wrlock(&hclient_lock);
				prev->next=tmp->next;
				pthread_rwlock_unlock(&hclient_lock);
			}
			else
			{
				printf("sai zei\n");
				pthread_rwlock_wrlock(&hclip_lock);
				prev->next=tmp->next;
				pthread_rwlock_unlock(&hclip_lock);
			}
		}
		free(tmp);
		printf("CHEGUEI AQUI OMG\n");
	}
	printf("OUTTA HERE\n");
}

void * thread_client(void* arg)
{
	int fd  = ((node*)arg)->fd;
	char* aux_clip = NULL;
	char* aux = NULL;
	char default_messages[10];
	char action='\0';
	int size=0, region=0;
	int nbytes =0 ;

	printf("NEW CLIENT THREAD ON!!! fd= %d\n", fd);
	while(1)
	{
		action='\0';
		region=100;
		size=0;
		printf("Client thread waiting...\n");
		memset(default_messages, (int) '\0', sizeof(char)*10);			
		nbytes = recv(fd, default_messages, sizeof(char)*10, 0);
		if (nbytes==0)
		{
			printf("Didn't receive a thing. The client has exited.\n");
			break;
		}
		else
		{
			printf("message from client %d: %s (c:r:s)\n", fd, default_messages);
			sscanf(default_messages, "%c:%d:%d", &action, &region, &size);
			if(action=='c') /* when received a copy order */
			{
				if(aux_clip!=NULL)
					free(aux_clip);

				if((aux_clip=(char*)malloc((size+1)*sizeof(char)))==NULL)
				{
					printf("Error malloc message. Unable to accept changes."); 
					memset(default_messages, (int) '\0', sizeof(char)*10);
					sprintf(default_messages, "ER"); /* in case of error returns ER */
					nbytes = send(fd, default_messages, sizeof(char)*10, 0);
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message\n");
						break;
					}
					continue;
				}
				memset(default_messages, (int) '\0', sizeof(char)*10);
				sprintf(default_messages, "OK"); /* ready para receber a mensagem */
				nbytes = send(fd, default_messages, sizeof(char)*10, 0);
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Unable to send message. Removing client.\n");
					break;
				}
				printf("SEND to client: OK\n");
				/* receives message */
				memset(aux_clip, (int) '\0', (size+1)*sizeof(char));
				printf("waiting for client... fd=%d\n", fd);
				nbytes = recv(fd, aux_clip, size*sizeof(char), 0);
				if (nbytes==0)
				{
					printf("Didnt receive a thing. Removing client.\n");
					break;
				}
				else
				{
					printf("RECEIVED FINAL MESSAGE FROM CLIENT: %s\n", aux_clip);
					/* !!!!!!!!!!!!!!!!!!!  difundir a noticia aqui para fora daqui !!!!!!!!!!!!!!!!!!!!!!!!! */
					pthread_rwlock_rdlock(&root_lock);
					if(root_fd!=-1)
					{
						pthread_rwlock_unlock(&root_lock);
						/* TESTE 
						pthread_rwlock_wrlock(&(clip_lock[region]));
						aux=clip[region];
						clip[region]=aux_clip;
						pthread_rwlock_unlock(&(clip_lock[region]));
						free(aux);
						FIM TESTE */
						send_root(aux_clip, size, region);
					}
					else
					{
						pthread_rwlock_unlock(&root_lock);
						pthread_rwlock_wrlock(&(clip_lock[region]));
						aux=clip[region];
						clip[region]=aux_clip;
						pthread_rwlock_unlock(&(clip_lock[region]));
						free(aux);
						send_clips(aux_clip, size, region);
					}
				} 
			}
			else if(action=='p')
			{
				pthread_rwlock_rdlock(&(clip_lock[region]));
				aux_clip=clip[region];
				pthread_rwlock_unlock(&(clip_lock[region]));
				memset(default_messages, (int) '\0', sizeof(char)*10);
				if(aux_clip==NULL) 
				{
					sprintf(default_messages, "%d", 0);
				    nbytes=send(fd, default_messages, sizeof(char)*10, 0); /* envia só um 0 */
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message. Removing client.\n");
						break;
					}
				}
				else
				{
					if(strlen(aux_clip)>(size))
					{
						nbytes = sprintf(default_messages, "%d", size);
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*(size),0);
						if (nbytes<=0) /*(nbytes<size) */
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
					else
					{
						nbytes = sprintf(default_messages, "%lu", strlen(aux_clip));
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*strlen(aux_clip),0);
						if (nbytes<=0) /* (nbytes<strlen(aux_clip)) */
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
				}
			}
			else if(action=='w')
			{
				pthread_rwlock_rdlock(&(clip_lock[region]));
				aux_clip=clip[region];
				pthread_rwlock_unlock(&(clip_lock[region]));
				memset(default_messages, (int) '\0', sizeof(char)*10);
				if(aux_clip==NULL) 
				{
					sprintf(default_messages, "%d", 0);
				    nbytes=send(fd, default_messages, sizeof(char)*10, 0); /* envia só um 0 */
					if (nbytes<=0) /*(nbytes<10)*/
					{
						printf("Unable to send message. Removing client.\n");
						break;
					}
				}
				else
				{
					if(strlen(aux_clip)>(size))
					{
						nbytes = sprintf(default_messages, "%d", size);
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*(size),0);
						if (nbytes<=0) /*(nbytes<size)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
					else
					{
						nbytes = sprintf(default_messages, "%lu", strlen(aux_clip));
						nbytes = send(fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
						if (nbytes<=0) /*(nbytes<10)*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
						nbytes = send(fd, aux_clip, sizeof(char)*strlen(aux_clip),0);
						if (nbytes<=0) /*(nbytes<strlen(aux_clip))*/
						{
							printf("Unable to send message. Removing client.\n");
							break;
						}
					}
				}
			}
			else if(action=='q')
			{
				/* quer dizer que este cliente vai sair -> há que fechar ligação */
				memset(default_messages, (int) '\0', sizeof(char)*10);
				sprintf(default_messages, "OK");
				nbytes = send(fd, default_messages, sizeof(char)*10, 0);
				if (nbytes<=0) /*(nbytes<10)*/
				{
					printf("Unable to send message. Removing client.\n");
					break;
				}
				printf("Here ?fd = %d\n", fd);
				remove_node(fd, 1);
				printf("And Here \n");
				close(fd);
				printf("and here too!\n");
				pthread_exit(NULL);
			}
			else /* action À toa */
				printf("Unable to proceed. Moving on.\n");
		}
	}
	close(fd);
	remove_node(fd, 0);
	pthread_exit(NULL);
}
void send_root(char* new, int size, int region)
{
	int nbytes=0;
	char default_messages[10];
	char * aux=NULL;

	printf("SENDING TO ROOT!!\n");
	memset(default_messages, (int) '\0', 10*sizeof(char));
	sprintf(default_messages, "%d:%d", region, size);
	/* root avisado */
	printf("SENT TO ROOT: %s\n", default_messages);
	nbytes = send(root_fd, default_messages, sizeof(char)*10, 0);
	if (nbytes<=0) /*(nbytes<10)*/
	{	
		printf("Root exited. Becoming root.\n");
		pthread_rwlock_rdlock(&root_lock);
		if(root_fd!=-1)
		{	
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&root_lock);
			close(root_fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&(clip_lock[region]));
			aux=clip[region];
			clip[region]=new;
			pthread_rwlock_unlock(&(clip_lock[region]));
			free(aux);
			send_clips(new, size, region);
		}
		else
			pthread_rwlock_unlock(&root_lock);
	}
	/* send the message */
	printf("SENT ROOT: %s\n", new);
	nbytes = send(root_fd, new, sizeof(char)*size, 0);
	if (nbytes==0)
	{
		printf("Root exited. Becoming root.\n");
		pthread_rwlock_rdlock(&root_lock);
		if(root_fd!=-1)
		{
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&root_lock);
			close(root_fd);
			root_fd=-1;
			pthread_rwlock_unlock(&root_lock);
			pthread_rwlock_wrlock(&(clip_lock[region]));
			aux=clip[region];
			clip[region]=new;
			pthread_rwlock_unlock(&(clip_lock[region]));
			free(aux);
			send_clips(new, size, region);

		}
		else
			pthread_rwlock_unlock(&root_lock);
	}
	
	printf("sent to root! %s\n", new);
}

void send_clips(char* new, int size, int region)
{
	printf("PASSEI POR AQUI E NAO DEVIA\n");
}