#ifndef CLIPLIB_H_INCLUDE
#define CLIPLIB_H_INCLUDE

#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <pthread.h>

#include "ext_var.h"
#include "clipboard.h"

int check_args(int, char* []);
int fetch_clip(int);
void * thread_root(void *);
void * thread_accept(void *);
node * add_node(int, int);
void remove_node(int, int);
void * thread_client(void*);
void send_root(char*, int, int);
void send_clips(char*, int, int);

#endif