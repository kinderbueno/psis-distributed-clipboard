#ifndef EXT_H_INCLUDE
#define EXT_H_INCLUDE

#include "clipboard.h"
#include "clip_lib.h"

#define OPER_CODE 100
#define FETCH_CODE 1
#define MAXPORT 64738
#define MINPORT 1024
#define MAXLEN 256
#define STDIN 0
#define STDLEN 100 
#define SOCK_ADDRESS "/tmp/sock_16" 

typedef struct Node /* for lists of established clips and clients */
{
	pthread_t thread;
	int fd;
	struct Node* next;
} node;

extern pthread_t root_thr;
extern pthread_t client_accept_thr;
extern pthread_t clip_accept_thr;
extern pthread_attr_t attr;
extern pthread_rwlock_t clip_lock[10];
extern pthread_rwlock_t hclip_lock; 
extern pthread_rwlock_t hclient_lock;
extern pthread_rwlock_t root_lock; 

extern char* clip[10];
extern int in_fd;
extern int out_fd;
extern int root_fd;
extern node* head_clip;
extern node* head_client;

#endif