#include "clipboard.h"

#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define MAXLEN 256
#define SOCK_ADDRESS "/tmp/sock_16"

int main()
{
	int fd=-1;
	char input[10], act='\0';
	int reg=0, err=0;
	size_t size=0;
	char* msg=NULL;
	char* fail=NULL;

	/* connects to the clipboard */
	fd = clipboard_connect(SOCK_ADDRESS);
	printf("Connected to: %d\n", fd); 

	if((msg=(char*)malloc(sizeof(char)*MAXLEN))==NULL)
	{
		printf("Error malloc. Aborting.\n");
		clipboard_close(fd);
		exit(-1);
	}
	memset(msg,(int)'\0', sizeof(char)*MAXLEN);
	size=MAXLEN; /*esperemos que com o maxlen funcione...*/
	printf("initial size=%lu\n", size);

	while(1)
	{
		act='\0';
		while(act!='c' && act!='C' && act!='p' && act!='P' && act!='q' && act!='Q' && act!='w' && act!='W')
		{
			printf("Action: copy or paste or wait? [c/p/w]\n(or type 'Q' to quit the program)\n");
			memset(input, (int) '\0', sizeof(char)*10);
			if((fail=fgets(input, 10, stdin))==NULL)
			{
				printf("Error fgets. Aborting.\n");
				clipboard_close(fd);
				free(msg);
				exit(-1);
			}
			act=input[0];
		}

		reg=20;
		/* when not quiting */
		if (act!='Q' && act!='q')
		{
			/* checks the region */
			while(reg<-1 || reg>10)
			{
				printf("Region? [1-10]\n");
				memset(input, (int) '\0', sizeof(char)*10);
				if((fgets(input, 10, stdin))==NULL)
				{
					printf("Error fgets. Aborting.\n");
					clipboard_close(fd);
					free(msg);
					exit(-1);
				}
				reg=atoi(input); 
				reg--; /* para ficar de 0 a 9 */
			}

			/* if copy */
			if(act=='c' || act=='C')
			{
				printf("Text? [pressing enter will end the input]\n");
				err = getline(&msg, &size, stdin); 
				if (err==-1)
				{
					perror("getline: ");
					clipboard_close(fd);
					free(msg);
					exit(-1);
				}
				else
				{
					if(!clipboard_copy(fd, reg, msg, strlen(msg)))
					{
						printf("Error copying to the clipboard. Aborting.\n");
						clipboard_close(fd);
						free(msg);
						exit(-1);
					}
					printf("Sucessfully copied! size = %lu\n", size);
					/*printf("DADOS:\n\t* regiao - %d;\n\t* acao - %c;\n\t* mensagem - %s\t* size - %d\n", reg, act, msg, size);*/
				}
			}
			/* if paste */
			else if(act=='p' || act!='P')
			{
				memset(msg,(int)'\0',sizeof(char)*size);
				/* infos sent is the fd and reg and size is the malloc of the msg. The msg will be filled with the paste (and reallocated if needed) */
				if(!clipboard_paste(fd, reg, msg, size)) 
				{
					printf("Error pasting to clipboard. Aborting.\n");
					free(msg);
					clipboard_close(fd);
					exit(-1);
				}
				printf("Received:\n %s\n -End Of Message\n", msg);
			}
			else if(act=='w' || act!='W')
			{
				memset(msg,(int)'\0',sizeof(char)*size);
				/* infos sent is the fd and reg and size is the malloc of the msg. The msg will be filled with the paste (and reallocated if needed) */
				if(!clipboard_wait(fd, reg, msg, size)) 
				{
					printf("Error waiting for clipboard. Aborting.");
					free(msg);
					clipboard_close(fd);
					exit(-1);
				}
				printf("Received:\n %s\n -End Of Message\n", msg);
			}
			else /* no copy or paste */
				printf("Unrecognized command. Please try again.\n");
		}
		else /* if quit */
		{
			printf("Quiting program.\n");
			clipboard_close(fd);
			free(msg);
			break;
		}
	}	
	printf("Cya! :)\n");
	exit(0);
}
