#define STDLEN 100 
#define SOCK_ADDRESS "/tmp/sock_16" /* vai ser alterado para "./" ?" */
/* #define SOCK_ADDRESS "./sock_16" */ /* operation not permited */
#define MAXPORT 64738
#define MINPORT 1024
#define STDIN 0
#define LISTEN_PORT 58000

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "clip_lib.h"

typedef struct Request 
{
	char action;
	int region;
	int size;
} request;

typedef struct Node /* for lists of established clips and clients */
{
	int fd;
	struct Node* next;
} node;

typedef struct Flood
{
	int region;
	int size;
	/* int ttl; */
}flood;

int clipboard_connect(char * clipboard_dir);
int clipboard_copy(int clipboard_id, int region, void *buf, size_t count);
int clipboard_paste(int clipboard_id, int region, void *buf, size_t count);
int clipboard_wait(int clipboard_id, int region, void *buf, size_t count);
void clipboard_close(int clipboard_id);

