#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "clipboard.h"
#include "clip_lib.h"

#define max(A,B) ((A)>=(B)?(A):(B))

 
int main(int argc, char* argv[])
{
	int in_fd=-1; /* for new clients */
	int out_fd=-1;	/* for new clips */
	int next_clip_fd=0;
	struct sockaddr_in out_addr; /* for new clips */
	struct sockaddr_in next_clip_addr; /* to the connected clip */ 
	struct sockaddr_un in_addr; /* for new clients */ 

	node* head_client=NULL; /* head of the clients list */
	node* head_clip=NULL; /* head of clips connecting to me list */

	char* clip[10];	/* clip itself */
	int size[10]; /* size of the allocated memory of the clip (diferent from strlen) */

	int nclients=0; /* number os clients of the clipboard */ /* ver se isto e mm necessario */
	int n_itcp=0; /* 0 if alone (single mode), more if connected mode */
	fd_set opensockets;
	int maxfd=0;
	flood *flood_msg=NULL;

	/* auxiliar variables */
	int auxfd=-1;
	node* new_node=NULL;
	char default_messages[10]; /* OK(ACK) and ERROR messages */
	request *req=NULL;
	socklen_t size_addr; 
	struct sockaddr_un client_aux_addr; /* for new connections */ 
	struct sockaddr_in clip_aux_addr;
	int err=0, i=0, nbytes=0;
	char act='\0';
	char msg[100];

	printf("c.1\n");
	err = check_args(argc, argv, &next_clip_fd, &next_clip_addr);
	if (err==-1)
		exit(-1);
	n_itcp=err;

	printf("c.2\n");

	if(next_clip_fd!=-1) 
	{
		/* adicionar o novo clip à lista de clips ligados */
		new_node=NULL;
		if ((new_node=(node*)malloc(sizeof(node)))==NULL)
		{
			printf("Error malloc node. Aborting program.");
			close(next_clip_fd);
			exit(-1);
		}
		memset(new_node,(int) '\0' ,sizeof(node));
		new_node->fd=next_clip_fd;
		new_node->next=NULL; 
		/* o novo clip fica então a cabeça da lista de clips ligados */
		head_clip=new_node;
	}
	printf("c.3\n");
	/* criar socket e defini-lo na estrutura e por o programa à escuta de clientes */
	in_fd=socket(AF_UNIX, SOCK_STREAM, 0);
	if (in_fd == -1)
	{
		perror("socket in_fd: ");
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}
	memset(&in_addr,(int) '\0' ,sizeof(in_addr));
	in_addr.sun_family = AF_UNIX;
	strcpy(in_addr.sun_path, SOCK_ADDRESS); /* isto pode estar a dar problemas aqui */
	unlink(SOCK_ADDRESS); /* para evitar erros de bind quando o programa vai abaixo */

	err = bind(in_fd, (struct sockaddr *)&in_addr, sizeof(in_addr));
	if(err == -1){
		perror("bind in_fd: ");
		close(in_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}
	err=listen(in_fd, 5); /* torna o socket passivo */
	if(err==-1)
	{
		perror("listen in_fd: ");
		close(in_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}

	printf("c.4\n");
	/* criar socket e defini-lo na estrutura e por o programa à escuta de clips*/
	out_fd=socket(AF_INET, SOCK_STREAM, 0);
	if (out_fd == -1)
	{
		perror("socket out_fd: ");
		close(in_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		free(head_clip);
		exit(-1);
	}
	memset(&out_addr,(int) '\0',sizeof(out_addr));
	out_addr.sin_family = AF_INET;
	out_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	out_addr.sin_port = htons ((u_short) LISTEN_PORT); /* FIXME: o que é que eu faço em relaçao a isto ? */

	err = bind(out_fd, (struct sockaddr *)&out_addr, sizeof(out_addr));
	if(err == -1)
	{
		perror("bind out_fd: ");
		close(in_fd);
		close(out_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}
	/* torna o socket passivo agrupando tentativas de ligaçao ate 5 valores */
	err=listen(out_fd, 5);
	if(err==-1)
	{
		perror("listen out_fd: ");
		close(out_fd);
		close(in_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}

	printf("c.4\n");
	/* criar estrutura de request e flood message */
	if ((req=(request*)malloc(sizeof(request)))==NULL)
	{
		printf("Error malloc request. Aborting program.");
		close(in_fd);
		close(out_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}
	memset(req,(int) '\0', sizeof(request));

	if ((flood_msg=(flood*)malloc(sizeof(flood)))==NULL)
	{
		printf("Error malloc flood message. Aborting program.");
		close(in_fd);
		close(out_fd);
		free(req);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}
	memset(flood_msg,(int) '\0', sizeof(flood));

	printf("c.5 and waiting.\n");

	memset(&client_aux_addr, (int) '\0', sizeof(client_aux_addr));
	size_addr=sizeof(struct sockaddr); 
	/* bloqueia até alguém cliente tente fazer connect */
	auxfd = accept(in_fd, (struct sockaddr *)&client_aux_addr, &size_addr);
	if (auxfd==-1)
	{
		perror("accept in_fd:");
		printf("*** passive=%d new=%d \n", in_fd, auxfd );
		close(in_fd);
		close(out_fd);
		free(req);
		free(flood_msg);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		exit(-1);
	}

	/* adicionar o novo clip à lista */
	new_node=NULL;
	if ((new_node=(node*)malloc(sizeof(node)))==NULL)
	{
		printf("Error malloc node. Aborting program.");
		close(in_fd);
		close(out_fd);
		if(next_clip_fd!=-1)
		{
			free(head_clip);
			close(next_clip_fd);
		}
		free(flood_msg);
		free(req);
		exit(-1);
	}
	memset(new_node,(int) '\0' ,sizeof(node));
	new_node->fd=auxfd;
	new_node->next=NULL; 
	/* o novo cliente fica então a cabeça da lista de clientes ligados */
	head_client=new_node;

	printf("connected: fd=%d\n", head_client->fd);

	/* por o size a zeros -> referente à memoria alocada e não ao tamanho real da palavra (poderá ser menor) */
	for(i=0;i<10;i++)
	{
		size[i]=0;
		clip[i]=NULL;
	}

	if(next_clip_fd!=-1)  /* este clip esta ligado a outro clip */
	{
		err = fetch_clip(head_clip->fd, clip, size);
		if (err==0)
		{
			printf("Impossible to fetch information about the clipboard information in this network. Aborting program.\n");
			free(head_clip);
			close(next_clip_fd);
			close(in_fd);
			close(out_fd);
			free(flood_msg);
			free(req);
			close(head_client->fd);
			free(head_client);
			exit(-1);
		}

	}
	printf("\n");
    printf("CLIPBOARD: (reg:malloc:strlen -> string)\n");
    for(i=0;i<10;i++)
    {
        if (size[i]!=0)
                printf("%d:%d->%s\n", i+1, size[i], clip[i]);
        else
                printf("%d:%d->\n", i+1, size[i]);
    }
    printf("\n");
	printf("Init done: conection to next clip, to the first client and regions syncronized.\n");

	while(1)
	{
		printf("LISTENING TO: ");
		/* Open sockets setting: reset and add the ones needed  */
		FD_ZERO(&opensockets); 
		FD_SET(STDIN, &opensockets); 
		maxfd = STDIN;
		if(head_client==NULL)
		{
			FD_SET(in_fd, &opensockets); /* fica a espera de novos clientes internos */
			maxfd = max(in_fd, maxfd);
			printf(" in_fd(%d) ", in_fd);
		}
		else
		{
			FD_SET(head_client->fd, &opensockets);
			maxfd = max(head_client->fd, maxfd);
			printf(" head_client(%d) ", head_client->fd);
		}
		if(head_clip==NULL) /* n tem ligações para outros clips */
		{
			FD_SET(out_fd, &opensockets);
			maxfd = max(in_fd, maxfd);
			printf(" out_fd(%d) ", out_fd);
		}
		else
		{
			FD_SET(head_clip->fd, &opensockets);
			maxfd = max(head_clip->fd, maxfd);
			printf(" head_clip(%d) ", head_clip->fd);
		}
		/* COMO ESTA AGORA APENAS PREMITE UM CLIP externo E UM CLIENTE */
		printf(".\n");


		/* Select: listen to open sockets and select the ones with messages	*/
		err = select(maxfd+1, &opensockets, (fd_set*)NULL, (fd_set *)NULL, (struct timeval*)NULL);
		if(err<1)
		{
			perror("Error selecting action. Aborting program.\n");
			exit(-1);
		}
		if(FD_ISSET(STDIN, &opensockets))  /* message from stdin */ 
		{
			printf("stdin detected!\n");
			fgets(msg, 5, stdin);
			sscanf(msg, "%c", &act); 
			if (act=='q') 
			{
				break;
			}
			if (act=='s')
			{
                printf("\n");
                printf("CLIPBOARD: (reg:malloc:strlen -> string)\n");
                for(i=0;i<10;i++)
                {
                    if (size[i]!=0)
                            printf("%d:%d->%s\n", i+1, size[i], clip[i]);
                    else
                            printf("%d:%d->\n", i+1, size[i]);
                }
                printf("\n");

			}
			else
				printf("Unkown command. Try again.\n");
		}
		else if(FD_ISSET(in_fd, &opensockets)) /* message from new client */
		{
			printf("in_fd detected!\n");
			auxfd=-1;
			memset(&client_aux_addr, (int) '\0', sizeof(client_aux_addr));
			auxfd = accept(in_fd, (struct sockaddr *)&client_aux_addr, &size_addr);
			if (auxfd==-1)
			{
				perror("accept: ");
				exit(-1);
			}

			if ((new_node=(node*)malloc(sizeof(node)))==NULL)
			{
				printf("Error malloc client. Aborting program.\n");
				exit(-1);
			}
			memset(new_node,(int) '\0', sizeof(node));
			new_node->fd=auxfd;
			new_node->next=head_client; /* o novo cliente fica então a cabeça da lista */
			head_client=new_node;

			printf("connected client - %d\n", auxfd);
		}
		else if(FD_ISSET(out_fd, &opensockets)) /* message from new clip */
		{
			printf("out_fd detected!\n");
			auxfd=-1;
			memset(&clip_aux_addr, (int) '\0', sizeof(clip_aux_addr));
			auxfd = accept(out_fd, (struct sockaddr *)&clip_aux_addr, &size_addr);
			if (auxfd==-1)
			{
				perror("accept: ");
				exit(-1);
			}

			if ((new_node=(node*)malloc(sizeof(node)))==NULL)
			{
				printf("Error malloc node. Aborting program.\n");
				exit(-1);
			}
			memset(new_node,(int) '\0', sizeof(node));
			new_node->fd=auxfd;
			new_node->next=head_clip; /* o novo cliente fica então a cabeça da lista */
			head_clip=new_node;

			printf("connected clip - %d \n", head_clip->fd);

		}
		else if(FD_ISSET(head_clip->fd, &opensockets)) /* message from the clip */
		{
			printf("head_clip detected!\n");
			memset(req, (int) '\0', sizeof(request));
			err = recv(head_clip->fd, req, sizeof(request), 0);
			if (err==0)
			{
				printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
				close(head_clip->fd);
				head_clip->fd=-1;
				free(head_clip);
				head_clip=NULL;
			}
			else
				printf("UPSIS! DEAD END!\n");
			/* aviso de q o backup vai desligar. Se calhar nao vai ser feito com a comunicaçao de request */

		}
		else if(FD_ISSET(head_client->fd, &opensockets)) /* message from the client */
		{
			printf("head_client detected!\n");
			memset(req, (int) '\0', sizeof(request));
			err = recv(head_client->fd, req, sizeof(request), 0);
			if (err==0)
			{
				printf("Didn't receive a thing. The client has exited unexpectadly.\n");
				close(head_client->fd);
				head_client->fd=-1;
				free(head_client);
				head_client=NULL;
			}
			else
			{
				printf("received smth\n");
				if(req->action=='c') /* when received a copy order */
				{
					printf("novo size=%d antigo size=%d\n", req->size, size[req->region]);
					if(size[req->region]<req->size) /* realloc if needed */ 
					{
						printf("é maior...\n");
						if (size[req->region]!=0)
							free(clip[req->region]);
						printf("free done\n");
						if((clip[req->region]=(char*)malloc(req->size*sizeof(char)))==NULL)
						{
							printf("Error malloc message. Aborting program."); 
							memset(default_messages, (int) '\0', sizeof(char)*10);
							sprintf(default_messages, "ER"); /* in case of error returns ER */
							nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0);
							/* shutdown */
							exit(-1);
						}
						size[req->region]=req->size;
						printf("size final=%d\n", req->size );
					}
					memset(default_messages, (int) '\0', sizeof(char)*10);
					sprintf(default_messages, "OK"); /* ready para receber a mensagem */
					nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0);
					printf("sent ok->%s\n", default_messages);
					/* receives message */
					memset(clip[req->region], (int) '\0', size[req->region]*sizeof(char));
					nbytes = recv(head_client->fd, clip[req->region], req->size*sizeof(char), 0);
					if (nbytes==0)
					{
						printf("Didnt receive a thing. Removing client.\n");
						close(head_client->fd);
						free(head_client);
						head_client=NULL;
					}
					else
					{
						/* !!!!!!!!!!!!!!!!!!!  difundir a noticia aqui para fora daqui !!!!!!!!!!!!!!!!!!!!!!!!! */
						if(head_clip!=NULL)
						{
							memset(msg, (int) '\0', sizeof(char)*strlen(msg));
							sprintf(msg, "%d:%lu", req->region, strlen(clip[req->region]));
							nbytes = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
							memset(default_messages, (int) '\0', sizeof(char)*10);
							nbytes = recv(head_clip->fd, default_messages, 10*sizeof(char), 0);
							if (nbytes==0)
							{
								printf("Didnt receive a thing. Removing clip.\n");
								close(head_clip->fd);
								free(head_clip);
								head_clip=NULL;
							}
							else if (!strcmp(default_messages, "OK"))
							{
								nbytes = send(head_clip->fd, clip[req->region], strlen(clip[req->region])*sizeof(char), 0);
								printf("Message succesfully sent to backup.\n"); 
							}
							else if (!strcmp(default_messages, "ER"))
							{
								printf("Clip doesn't have space. Not sending and removing clip. \n");
								close(head_clip->fd);
								free(head_clip);
								head_clip=NULL;
							}
							else
								printf("Smth happened. Expected an OK/ER and received - %s - instead.\n", default_messages );
						}
					}
						
				}
				else if(req->action=='p')
				{
					if(size[req->region]==0 || strlen(clip[req->region])==1) 
					{
						/* envia uma zero de volta porque a posição de memória não tem nada, 1 pq strten contem barra 0*/
						memset(default_messages, (int) '\0', sizeof(char)*10);
					    nbytes = send(head_client->fd, default_messages, sizeof(char), 0); /* envia só um \0 */ 
					}
					else
					{
						memset(default_messages, (int) '\0', sizeof(char)*10);
						if(strlen(clip[req->region])>(req->size))
						{
							nbytes = sprintf(default_messages, "%d", req->size);
							nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
							nbytes = send(head_client->fd, clip[req->region], sizeof(char)*(req->size),0);
						}
						else
						{
							nbytes = sprintf(default_messages, "%lu", strlen(clip[req->region]));
							printf("size[i]= %d req->size = %d: msg: %s\n", size[req->region], req->size,  clip[req->region]); 
							nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
							nbytes = send(head_client->fd, clip[req->region], sizeof(char)*strlen(clip[req->region]),0);
						}	
					 /*
						i=strlen(clip[req->region]);
						nbytes = send(head_client->fd, &i , sizeof(int), 0);
						memset(default_messages, (int) '\0', sizeof(char)*10);
						nbytes = recv(head_client->fd, default_messages, 10*sizeof(char), 0);
						if (nbytes==0)
						{
							printf("Didn't receive a thing. Removing client.\n");
							close(head_client->fd);
							free(head_client);
							head_client=NULL;
						}
						else if (!strcmp(default_messages, "OK"))
						{
							nbytes = send(head_client->fd, clip[req->region], strlen(clip[req->region])*sizeof(char), 0);
							printf("Message sent.\n"); 
						}
						else if (!strcmp(default_messages, "ER"))
						{
							printf("Client doesn't have space. Not sending and removing client. \n");
							close(head_client->fd);
							free(head_client);
							head_client=NULL;
						}
						else 
							printf("Smth happened. Expected an OK/ER and received - %s - instead.\n", default_messages );
						*/
					}
				}
				else if(req->action=='w')
				{

					if(size[req->region]==0 || strlen(clip[req->region])==1) 
					{
						/* envia uma zero de volta porque a posição de memória não tem nada, 1 pq strten contem barra 0*/
						memset(default_messages, (int) '\0', sizeof(char)*10);
					    nbytes = send(head_client->fd, default_messages, sizeof(char), 0); /* envia só um \0 */ 
					}
					else
					{
						memset(default_messages, (int) '\0', sizeof(char)*10);
						if(strlen(clip[req->region])>(req->size))
						{
							nbytes = sprintf(default_messages, "%d", req->size);
							nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
							nbytes = send(head_client->fd, clip[req->region], sizeof(char)*(req->size),0);
						}
						else
						{
							nbytes = sprintf(default_messages, "%lu", strlen(clip[req->region]));
							nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0); /* ATENÇAO DEIXAR ESTE 10 e NAO MEXER PQ SENAO DA sHIT */
							nbytes = send(head_client->fd, clip[req->region], sizeof(char)*(strlen(clip[req->region])),0);
						}
					}
				}
				else if(req->action=='q')
				{
					/* quer dizer que este cliente vai sair -> há que fechar ligação */
					/* FIXME: procurar o cliente na lista e elimina-lo, depois enviar um OK, depois ha que o tirar do select (se calhar faz se isso automaticamente)*/
					memset(msg, (int) '\0', sizeof(char)*strlen(msg));
					sprintf(default_messages, "OK");
					nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0);
					printf("sent ok\n");
					close(head_client->fd);
					free(head_client);
					head_client=NULL;
				}
				else /* action À toa */
				{
					printf("DEAD END !! did nothing !!\n");
					/*memset(msg, (int) '\0', sizeof(char)*strlen(msg));
					sprintf(default_messages, "ER");
					nbytes = send(head_client->fd, default_messages, sizeof(char)*10, 0);
					printf("sent error!!!\n");
					*/
				}
			}
		}		
	}
	/* AFTER BREAK: eliminar a estrutura do request*/
	free(req);
	free(flood_msg);
	/* FAZER O FREE DAS LISTAS */
	close(out_fd);
	close(in_fd);

	if(head_client!=NULL) 
	{
		close(head_client->fd);
		free(head_client);
	}
	if(head_clip!=NULL) 
	{
		close(head_clip->fd);
		free(head_clip);
	}
	/* free das estruturas abertas quando exit */
	for(i=0;i<10;i++)
		if(size[i]!=0)
			free(clip[i]);
		
	exit(0);
	
}
