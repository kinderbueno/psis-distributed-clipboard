#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h> 

#define OPER_CODE 100
#define FETCH_CODE 1
#define MAXPORT 64738
#define MINPORT 1024
#define MAXLEN 100000


int check_args(int, char* [], int*, struct sockaddr_in*);
int fetch_clip(int, char*[10], int[10]);