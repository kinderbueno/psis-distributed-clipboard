#include "clipboard.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct sockaddr_un clip_addr;
request* req=NULL;

/* sem issues */
int clipboard_connect(char * clipboard_dir)
{
	int err=0, fd=0;

	fd=socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("socket: ");
		return -1;
	}

	memset(&clip_addr,(int) '\0' ,sizeof(clip_addr));
	clip_addr.sun_family = AF_UNIX;
	strcpy(clip_addr.sun_path, clipboard_dir);

	err=connect(fd, (struct sockaddr *) &clip_addr, sizeof(clip_addr));
	if(err == -1)
	{
		perror("Error connecting\n");
		return -1;
	}

	if ((req=(request*)malloc(sizeof(request)))==NULL)
	{
		printf("Unable to malloc request");
		return -1;
	}

	return fd;
}

int clipboard_copy(int clipboard_id, int region, void *buf, size_t count)
{
	int nbytes=0;
	int flag=0;
	char msg[10];

	memset(msg,(int) '\0' ,sizeof(char)*10);
	memset(req,(int) '\0' ,sizeof(request));

	if(region>=0 && region<10)	req->region=region;
	else return 0;

	printf("count==\n");
	/* printf("CHAR=%c",((char*)buf)[count-1]); */ 
	if ( ((char*)buf)[count-1]=='\n') 
	{
		printf("muahahah\n");
		if (count==1) return 0;
		((char*)buf)[count-1]='\0';
		count=count-1;
		flag=1;
	}
	printf("2\n");
	req->size=count;
	req->action='c';

	nbytes = send(clipboard_id, req, sizeof(request), 0); /* envia request */
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recebe 1º OK ?*/
	if (nbytes==0)
	{
		printf("Error receiving 1st OK. No chages were made. (Clipboard quited unexpectedly).\n");
		return 0;
	}
	else if(!strcmp(msg,"OK")) /* 1º OK recebido */
	{
		nbytes=0;
		nbytes = send(clipboard_id, (char*) buf, sizeof(char)*count, 0); /* envia texto */
		/* if send merda, return 0 */
		printf("nbytes=%d\n", nbytes);
		if (flag==0)
			return nbytes+1;
		else 
			return nbytes;
	}
	else if(!strcmp(msg,"ER"))
	{
		printf("Malloc error on the clip side.\n");
		return 0;
	}
	return 0;
}

int clipboard_paste(int clipboard_id, int region, void *buf, size_t count)
{	
	int nbytes=0, size=0;
	char msg[10];

	memset(req,(int) '\0' ,sizeof(request));

	if(region>=0 && region<10)	req->region=region;
	else return 0;
	req->size=count;
	req->action='p';
	printf("SENDING request, count = %lu\n", count);
	
	nbytes = send(clipboard_id, req, sizeof(request), 0);
		printf("RECEIVE SIZE\n");
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recepçao da mensagem */
	if(nbytes==0)
	{
		printf("Nothing was received... Clipboard exited unexpectedly.\n");
		return 0;
	}
	else
	{

		/* PROBLEMA A ENVIAR AS COISAS PARA O PASTE; APARECEM DEMASIADO PEQUENAS */
		sscanf(msg, "%d", &size);
		printf("!!!!! SIZE=%d !!!!!count=%lu\n", size, count);
		if (size>0)
		{
			memset(buf,(int) '\0' ,sizeof(char)*count);
			/*nbytes = recv(clipboard_id, buf, sizeof(char)*size, 0); *//* recepçao da mensagem */
			nbytes = recv(clipboard_id, buf, size+1, 0);
			if(nbytes==0)
			{
				printf("Nothing was received... Clipboard exited unexpectedly.\n");
				return 0;
			}
			else
			{
				printf("tamanho = %d MENSAGEM RECEBIDA = %s\n", nbytes, (char*) buf);
				return size;
			}
		}
		else 
			return 1; /* FIXME: posso fazer isto ??? */
	}
}

int clipboard_wait(int clipboard_id, int region, void *buf, size_t count) 
{	
	int nbytes=0, size=0;
	char msg[10];

	memset(req,(int) '\0' ,sizeof(request));

	if(region>=0 && region<10)	req->region=region;
	else return 0;
	req->size=count;
	req->action='p';
	
	nbytes = send(clipboard_id, req, sizeof(request), 0);
	memset(msg,(int) '\0' ,sizeof(char)*10);
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recepçao da mensagem */
	if(nbytes==0)
	{
		printf("Nothing was received... Clipboard exited unexpectedly.\n");
		return 0;
	}
	else
	{
		sscanf(msg, "%d", &size);
		if (size>0)
		{
			memset(buf,(int) '\0' ,sizeof(char)*count);
			nbytes = recv(clipboard_id, buf, sizeof(char)*size, 0); /* recepçao da mensagem */
			if(nbytes==0)
			{
				printf("Nothing was received... Clipboard exited unexpectedly.\n");
				return 0;
			}
			else
			{
				*((char*)buf+size)='\0'; /* verificar isto: com o memset ja n devera ser preciso*/
				return size;
			}
		}
		else 
			return 1; /* FIXME: posso fazer isto ??? */
	}
}

void clipboard_close(int clipboard_id)
{
	char msg[10];
	int nbytes=0;

	memset(req,(int) '\0' ,sizeof(request));
	req->action='q'; 

	nbytes = send(clipboard_id, req, sizeof(request), 0);
	/* if send merda entao não enviar e sair na mesma */
	memset(msg,(int) '\0' ,sizeof(char)*10);
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* deverá receber um OK */
	if(nbytes==0)
		printf("Nothing was received... Clipboard exited unexpectedly.\n");
	else
	{
		if(!strcmp(msg,"ER"))
			printf("Error. Bad request probably ?\n Closing the program anyway.\n"); /* vale a pena enviar outra vez ? */
		else if(!strcmp(msg,"OK")) /* se for OK */
			printf("Clip informed that we're quiting.\n"); 
	}
	close(clipboard_id);
	free(req);
}