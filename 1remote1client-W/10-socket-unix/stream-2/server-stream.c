#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <unistd.h>

#include "sock_stream.h"

int main(){
	struct sockaddr_un local_addr;
	struct sockaddr_un client_addr;
	 socklen_t size_addr;

	char buff[100];
	int nbytes;
	
	int sock_fd= socket(AF_UNIX, SOCK_STREAM, 0);
	
	if (sock_fd == -1){
		perror("socket: ");
		exit(-1);
	}
	

	local_addr.sun_family = AF_UNIX;
	strcpy(local_addr.sun_path, SOCK_ADDRESS);
	int err = bind(sock_fd, (struct sockaddr *)&local_addr, sizeof(local_addr));
	if(err == -1) {
		perror("bind");
		exit(-1);
	}
	printf(" socket created and binded \n");
	
	listen(sock_fd, 5);
	
	printf("Ready to accept connections\n");
	
	int client_fd= accept(sock_fd, (struct sockaddr *) & client_addr, &size_addr);
	
	printf("Accepted one connection from %s \n", client_addr.sun_path);
	nbytes = recv(client_fd, buff, 100, 0);
	printf("received %d bytes --- %s ---\n", nbytes, buff);
	
	sprintf(buff, "reply 1 to %s", client_addr.sun_path);
    nbytes = send(client_fd, buff, strlen(buff)+1, 0);
    printf("replying %d bytes\n", nbytes);

	//sleep(1);
	nbytes = recv(client_fd, buff, 100, 0);
	printf("received %d bytes --- %s ---\n", nbytes, buff);
	sprintf(buff, "reply 2 to %s", client_addr.sun_path);
    nbytes = send(client_fd, buff, strlen(buff)+1, 0);
    printf("replying %d bytes\n", nbytes);

	//sleep(1);	
	nbytes = recv(client_fd, buff, 100, 0);
	printf("received %d bytes --- %s ---\n", nbytes, buff);
	sprintf(buff, "reply 3 to %s", client_addr.sun_path);
    nbytes = send(client_fd, buff, strlen(buff)+1, 0);
    printf("replying %d bytes\n", nbytes);

	close(sock_fd);
	//unlink(SOCK_ADDRESS);
	exit(0);
}
