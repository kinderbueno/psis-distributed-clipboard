#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "clipboard.h"

#define R_LISTEN_PORT 59000
#define STDIN 0
#define OPER_CODE 100
#define FETCH_CODE 1

#define max(A,B) ((A)>=(B)?(A):(B))
 
int main(int argc, char* argv[])
{
	int out_fd=0;	/* for new clips */ 
	struct sockaddr_in out_addr;
	node* head_clip=NULL; /* head of clips connecting to me list */

	char* clip[10];	/* clip itself */
	int size[10]; /* size of the allocated memory of the clip (diferent from strlen) */

	flood *flood_msg=NULL;
	fd_set opensockets;
	int maxfd=0;

	/* auxiliar variables */
	int auxfd=0;
	socklen_t size_addr; 
	struct sockaddr_in aux_addr; /* for new connections */ 
	node* new_client=NULL;
	int err=0, i=0;
	char act='\0';
	char msg[MAXLEN], aux[10];
	int region=0, count=0;


	/* INITS*/

	/* por o size a zeros -> referente à memoria alocada e não ao tamanho real da palavra (poderá ser menor) */
	for(i=0;i<10;i++)
	{
		size[i]=0;
		clip[i]=NULL;
	}

	/* TESTE */
	for(err=0;err<2;err++)
	{
		memset(aux,(int)'\0',sizeof(char)*strlen(aux));
		if(err==0) strcpy(aux,"ola");
		else strcpy(aux,"adeus");
		for(i=err;i<10;i+=2)
		{
			size[i]=strlen(aux);
			if ((clip[i]=(char*)malloc(sizeof(char)*size[i]))==NULL)
			{
				printf("Error malloc message. Aborting.");
				exit(-1);
			}
			memset(clip[i],(int)'\0',sizeof(char)*size[i]);
			strcpy(clip[i], aux);
		}
	}
	/* FIM DO TESTE */


	printf("r.1\n");

	if ((flood_msg=(flood*)malloc(sizeof(flood)))==NULL)
	{
		printf("Error malloc flood message. Aborting.");
		exit(-1);
	}
	memset(flood_msg,(int)'\0',sizeof(flood));
	printf("r.2\n");

	/* criar socket e defini-lo na estrutura e por o programa à escuta */
	out_fd=socket(AF_INET, SOCK_STREAM, 0);
	if (out_fd == -1)
	{
		perror("socket: ");
		exit(-1);
	}
	/* mmset de coisas !! addrs e outros */
	memset(&out_addr,(int) '\0',sizeof(out_addr));
	out_addr.sin_family = AF_INET;
	out_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	out_addr.sin_port = htons ((u_short) R_LISTEN_PORT); /* FIXME: o que é que eu faço em relaçao a isto  */

	err = bind(out_fd, (struct sockaddr *)&out_addr, sizeof(out_addr));
	if(err == -1)
	{
		perror("bind: ");
		close(out_fd);
		exit(-1);
	}
	/* torna o socket passivo  agrupando tentativas de ligaçao ate 5 valores */
	listen(out_fd, 5);
	printf("r.3\n");
	printf("IP: 192.168.1.78\nPORT: %d\n", R_LISTEN_PORT);
	/* bloqueia ate alguem tentar fazer connect */
	auxfd = accept(out_fd, (struct sockaddr *)&aux_addr, &size_addr);
	if (auxfd==-1)
	{
		perror("accept out_fd");
		close(out_fd);
		exit(-1);
	}

	/* adicionar o novo clip à lista */
	new_client=NULL;
	if ((new_client=(node*)malloc(sizeof(node)))==NULL)
	{
		printf("Error malloc client. Aborting.");
		close(out_fd);
		exit(-1);
	}
	memset(new_client,(int) '\0' ,sizeof(node));
	new_client->fd=auxfd;
	new_client->next=NULL; 
	/* o novo cliente fica então a cabeça da listade clips ligados */
	head_clip=new_client;

	memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);

	printf("connected. out_fd=%d head_clip->fd=%d\n", out_fd, head_clip->fd);

	while(1)
	{
		/* Open sockets setting: reset and add the ones needed  */
		FD_ZERO(&opensockets); 
		FD_SET(STDIN, &opensockets); 
		maxfd = STDIN;

		if(head_clip!=NULL)
		{
			printf("Connection table full. head_clip->fd=%d.\n", head_clip->fd);
			FD_SET(head_clip->fd, &opensockets);
			maxfd = max(head_clip->fd, maxfd);
		}
		else /* para alguém se ligar se nao houver ninguem ligado ao backup */
		{
			printf("Listening to new connections. out_fd=%d\n", out_fd);
			FD_SET(out_fd, &opensockets);
			maxfd = max(out_fd, maxfd);
		}

		/* Select: listen to open sockets and select the ones with messages	*/
		err = select(maxfd+1, &opensockets, (fd_set*)NULL, (fd_set *)NULL, (struct timeval*)NULL);
		if(err<1)
		{
			perror("Error selecting action. Aborting program.\n");
			close(out_fd);
			exit(-1);
		}
		if(FD_ISSET(STDIN, &opensockets))  /* message from stdin */ 
		{
			memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);
			fgets(msg, 5, stdin); 
			sscanf(msg, "%c", &act); 
			if (act=='q') 
			{
				close(out_fd);
				if(head_clip!=NULL)
					close(head_clip->fd);
				break;
			}
			else if (act=='s') 
				printf("IP: 192.168.1.78\nPORT: %d\n", R_LISTEN_PORT);
			else if (act=='c')
			{
				printf("\n");
                printf("CLIPBOARD: (reg:malloc:strlen -> string)\n");
                for(i=0;i<10;i++)
                {
                    if (size[i]!=0)
                            printf("%d:%d->%s\n", i+1, size[i], clip[i]);
                    else
                            printf("%d:%d->\n", i+1, size[i]);
                }
                printf("\n");
			}
			else
				/* POR A OPÇAO DE SHOW */
				printf("Unkown command. Try again.\n");
		}
		else if(FD_ISSET(head_clip->fd, &opensockets))  /* message from the the local clip */ 
		{
			printf("Yes, really. I'm full.\n");
			memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);
			err = recv(head_clip->fd, msg, sizeof(char)*MAXLEN, 0); /* n de bytes */
			if (err==0)
			{
				printf("Didn't receive a thing. The client has exited unexpectadly.\n");
				close(head_clip->fd);
				head_clip->fd=-1;
				free(head_clip);
				head_clip=NULL;
			}
			else /* received smth */
			{
				sscanf(msg, "%d:%d", &region, &count);
				printf("MESSAGE RECEIVED:\n");
				printf("%s\n", msg);
				if (region==OPER_CODE)
				{
					if(count==FETCH_CODE)
					{
						memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);
						for(i=0;i<10;i++)
						{
							if(clip[i]!=NULL)
								sprintf(aux,"%lu", strlen(clip[i]));
							else
								sprintf(aux,"%d", 0);
							strcat(msg,aux);
							if(i!=9) 
							{
								sprintf(aux,":");
								strcat(msg,aux);
							}	
						}
						printf("msg->%s\n", msg);
						err = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
						memset(msg, (int) '\0', sizeof(char)*MAXLEN);
						err = recv(head_clip->fd, msg, sizeof(char)*MAXLEN, 0); 
						if (err==0)
						{
							printf("Didnt receive a thing. The client has exited unexpectadly.\n");
							close(head_clip->fd);
							head_clip->fd=-1;
							free(head_clip);
							head_clip=NULL;
						}
						else if(!strcmp(msg,"OK"))
						{
							printf("VOU COMECAR A ENVIAR!\n");
							for(i=0;i<10;i++)
							{
								printf("Nº %d\n", i+1 );
								err = send(head_clip->fd, clip[i], sizeof(char)*strlen(clip[i]), 0);
							}
						}
						else if(!strcmp(msg,"ER"))
						{
							printf("The clip doesn't have space available. Not sending and removing clip.\n");
							close(head_clip->fd);
							head_clip->fd=-1;
							free(head_clip);
							head_clip=NULL;
						}
						else
							printf("WHAT WAS THIS! %s\n", msg );
					}
					else
					{
						printf("Unkown operation.\n");
						memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);
						sprintf(msg,"ER");
						err = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
					}
				}
				else if (region>=0 && region<10) /* valores a contar do zero */
				{
					if(count>=size[region])
					{
						if(size[region]!=0)
							free(clip[region]);
						if((clip[region]=(char*)malloc(sizeof(char)*size[region]))==NULL)
						{
							printf("Error during malloc. Aborting program.\n");
							memset(msg,(int) '\0' ,sizeof(char)*MAXLEN);
							sprintf(msg,"ER");
							err = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
							/* shutdown */
							close(out_fd);
							exit(-1);
						}
					}
					memset(msg,(int) '\0' ,sizeof(char)*strlen(msg));
					sprintf(msg,"OK");
					err = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
					memset(msg,(int) '\0' ,sizeof(char)*strlen(msg));
					err = recv(head_clip->fd, msg, sizeof(char)*MAXLEN, 0); 
					if (err==0)
					{
						printf("didnt receive a thing. -> The client has exited unexpectadly.\n");
						close(head_clip->fd);
						head_clip->fd=-1;
						free(head_clip);
						head_clip=NULL;
					}
					else 
						sscanf(msg,"%s",clip[region]);
				}
				else
				{
					printf("Unkown operation.\n");
					memset(msg,(int) '\0' ,sizeof(char)*strlen(msg));
					sprintf(msg,"ER");
					err = send(head_clip->fd, msg, sizeof(char)*strlen(msg), 0);
				}
			}
		}
		else if(FD_ISSET(out_fd, &opensockets))  /* message from the passive socket */ 
		{
			printf("Yes.really. I'm listenning.\n");
			auxfd = accept(out_fd, (struct sockaddr *)&aux_addr, &size_addr);
			if (auxfd==-1)
			{
				perror("accept out_fd: ");
				close(out_fd);
				exit(-1);
			}

			/* adicionar o novo clip à lista */
			new_client=NULL;
			if ((new_client=(node*)malloc(sizeof(node)))==NULL)
			{
				printf("Error malloc client. Aborting.");
				close(out_fd);
				exit(-1);
			}
			memset(new_client,(int) '\0' ,sizeof(node));
			new_client->fd=auxfd;
			new_client->next=NULL; 
			/* o novo cliente fica então a cabeça da listade clips ligados */
			head_clip=new_client;

			printf("connected\n");
		}		
	}
	printf("breaked\n");
	/* eliminar a estrutura do request*/
	free(flood_msg);

	/* free das estruturas abertas quando exit */
	for(i=0;i<10;i++)
		if(size[i]!=0)
			free(clip[i]);

	exit(0);
	
}
