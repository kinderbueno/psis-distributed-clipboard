#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "clip_lib.h"

int check_args(int c, char* v[], int* next_clip_fd, struct sockaddr_in* next_clip_addr)
{
	char ip[30];
	int err=0, port=0;	

	memset(next_clip_addr, (int) '\0', sizeof(*next_clip_addr));
	memset(ip, (int) '\0', sizeof(char)*30);
	(*next_clip_fd)=-1;

	if (c==1)
		return 0; /* clipboard is alone */
	else if (c==4)
	{
		/* scans through the arguments and verifies them before adding to the stucture 	*/
		if(!strcmp("-c", v[1]))
		{
			if(sscanf(v[2], "%s", ip)!=1 || !inet_aton(ip, &(next_clip_addr->sin_addr)))
			{
				printf("Invalid given IP. Aborting program.\n");
				return -1;
			}
			if(sscanf(v[3], "%d", &port)!=1 || !(port>=MINPORT && port<=MAXPORT) )
			{
				printf("Invalid given port. Aborting program. It should be a integer between %d and %d.\n", MINPORT, MAXPORT);
				return -1;
			}

			/* arguments are now verified  */
			next_clip_addr->sin_family = AF_INET;
			next_clip_addr->sin_port = htons((u_short) port);
			/* central_server->sin_addr.s_addr = server_ip.s_addr; ---> isto ja esta feito acima */

			*next_clip_fd = socket(AF_INET, SOCK_STREAM, 0);
			if (*next_clip_fd == -1){
				perror("socket: ");
				return -1;
			}

			err = connect(*next_clip_fd,(struct sockaddr *)next_clip_addr, sizeof(*next_clip_addr));
			if(err==-1)
			{
				printf("Error connecting to next clipboard. Aborting program.\n");
				close(*next_clip_fd);
				return -1;
			}
			return 1;
		}
		else 
		{
			printf("Invalid number of arguments. Use the following:\n./clipboard [-c ip_next port_next]\n");
			return -1;
		}	
	}
	else
	{
		printf("Invalid arguments. Use the following:\n./clipboard [-c ip_next port_next]\n");
		return -1;
	}
}


int fetch_clip(int fd, char* clip[10], int size[10])
{
	char msg[MAXLEN];
	char* aux_clip[10];
	int aux_size[10];
	int i=0, j=0, err=0;
	
	memset(msg, (int) '\0', sizeof(char)*MAXLEN);
	/* send fetch request */
	sprintf(msg, "%d:%d", OPER_CODE, FETCH_CODE);
	err = send(fd, msg, sizeof(char)*strlen(msg), 0);

	memset(msg, (int) '\0', sizeof(char)*strlen(msg));
	/* recieve sizes */
	err = recv(fd, msg, sizeof(char)*MAXLEN, 0); /* n de bytes */
	if (err==0)
	{
		printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
		return 0;
	}
	else
		sscanf(msg, "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d", 
				aux_size, aux_size+1, aux_size+2, aux_size+3, aux_size+4,
				aux_size+5, aux_size+6, aux_size+7, aux_size+8, aux_size+9); /* posso fazer isto ou tenho de multiplicar por sizeof(char) */

	printf("AQUI!!\n");
	/* try to allocate all memory */
	for(i=0; i<10; i++)
	{
		aux_clip[i]=NULL;
		if((aux_clip[i]=(char*)malloc(sizeof(char)*aux_size[i]))==NULL)
		{
			printf("Error malloc. Aborting program.\n");
			memset(msg, (int) '\0', sizeof(char)*strlen(msg));
			sprintf(msg, "ER");
			err = send(fd, msg, sizeof(char)*strlen(msg), 0);
			for(j=0;j<i;j++)
				free(aux_clip[j]);
			return 0;
		}
	}
	memset(msg, (int) '\0', sizeof(char)*strlen(msg));
	sprintf(msg, "OK");
	err = send(fd, msg, sizeof(char)*strlen(msg), 0);
	printf("AQUI 2!!!\n");
	/* receber as palavras */
	for (i=0;i<10;i++)
	{
		memset(aux_clip[i], (int) '\0', sizeof(char)*aux_size[i]);
		err = recv(fd, aux_clip[i] , sizeof(char)*aux_size[i], 0); /* n de bytes */
		if (err==0)
		{
			printf("Didn't receive a thing. The clip has exited unexpectadly.\n");
			for(j=0;j<i;j++)
				free(aux_clip[j]);
			return 0;
		}
	}
	printf("AQUI 3 !!!\n");

	/* se correu tudo bem: precaução para não ficarem coisas a meio */
	for (i=0;i<10;i++)
	{
		size[i]=aux_size[i];
		clip[i]=aux_clip[i];
	}
	printf("OUTA HERE!!\n");
	return 1;
}