#include "clipboard.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct sockaddr_un clip_addr;

/* sem issues */
int clipboard_connect(char * clipboard_dir)
{
	int err=0, fd=0;

	fd=socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("socket: ");
		return -1;
	}

	memset(&clip_addr,(int) '\0' ,sizeof(clip_addr));
	clip_addr.sun_family = AF_UNIX;
	strcpy(clip_addr.sun_path, clipboard_dir);

	err=connect(fd, (struct sockaddr *) &clip_addr, sizeof(clip_addr));
	if(err == -1)
	{
		perror("Error connecting\n");
		return -1;
	}

	printf("accept FD=%d, accepted fd =%d\n", fd, err);
	return fd;
}

int clipboard_copy(int clipboard_id, int region, void *buf, size_t count)
{
	int nbytes=0;
	char act='\0';
	char msg[10];

	if(!(region>=0 && region<10))
		return 0;

	act='c';
	memset(msg,(int) '\0', sizeof(char)*10);
	sprintf(msg, "%c:%d:%lu", act, region, count);

	printf("fd=%d, msg:%s\n",clipboard_id,msg);
	nbytes = send(clipboard_id, msg, sizeof(char)*10, 0);
	printf("NBYTES=%d\n", nbytes);
	perror("send: ");
	if (nbytes<=0) 
	{
		printf("Unable to send mensage.\n");
		return 0;
	}
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0);
	if (nbytes==0)
	{
		printf("Clipboard exited. No changes were made.\n");
		return 0;
	}
	else if(!strcmp(msg,"OK")) /* 1º OK recebido */
	{
		nbytes=0;
		nbytes = send(clipboard_id, (char*) buf, sizeof(char)*count, 0); /* envia texto */
		if (nbytes<=0) /*(nbytes<count)*/
		{
			printf("Unable to send mensage.\n");
			return 0;
		}
		return nbytes;
	}
	else if(!strcmp(msg,"ER"))
	{
		printf("Malloc error on the clip side. No changes were made.\n");
		return 0;
	}
	return 0;
}

int clipboard_paste(int clipboard_id, int region, void *buf, size_t count)
{	
	int nbytes=0;
	char msg[10];
	char act='\0';
	int size=0;

	if(!(region>=0 && region<10) || count == 0)
		return 0;
	
	act='p';
	sprintf(msg, "%c:%d:%lu", act, region, count);
	nbytes = send(clipboard_id, msg, sizeof(char)*10, 0);
	if (nbytes<=0) /* (nbytes<10) */
	{
		printf("Unable to send mensage.\n");
		return 0;
	}
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recepçao do size*/
	if(nbytes==0)
	{
		printf("Clipboard exited unexpectedly. Nothing was received.\n");
		return 0;
	}
	else
	{
		sscanf(msg, "%d", &size);
		if (size>0)
		{
			memset(buf,(int) '\0' ,sizeof(char)*count);
			nbytes = recv(clipboard_id, buf, size, 0);
			if(nbytes==0)
			{
				printf("Clipboard exited. Nothing was received.\n");
				return 0;
			}
			else
				return size;
		}
		else
		{
			printf("Nothing on the selected region.\n");
			return 0;
		} 
	}
	return 0;
}

int clipboard_wait(int clipboard_id, int region, void *buf, size_t count) 
{	
	int nbytes=0;
	char msg[10];
	char act='\0';
	int size=0;


	if(!(region>=0 && region<10))
		return 0;
	
	act='p';
	sprintf(msg, "%c:%d:%lu", act, region, count);
	nbytes = send(clipboard_id, msg, sizeof(char)*10, 0);
	if (nbytes<=0) /*(nbytes<10) */
	{
		printf("Unable to send mensage.\n");
		return 0;
	}
	nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* recepçao do size*/
	if(nbytes==0)
	{
		printf("Clipboard exited unexpectedly. Nothing was received.\n");
		return 0;
	}
	else
	{
		sscanf(msg, "%d", &size);
		if (size>0)
		{
			memset(buf,(int) '\0' ,sizeof(char)*count);
			nbytes = recv(clipboard_id, buf, size, 0);
			if(nbytes==0)
			{
				printf("Clipboard exited. Nothing was received.\n");
				return 0;
			}
			else
				return size;
		}
		else
		{
			printf("Nothing on the selected region.\n");
			return 0;
		} 
	}
	return 0;
}

void clipboard_close(int clipboard_id)
{
	char msg[10];
	int nbytes=0;

	memset(msg,(int) '\0' ,sizeof(char)*10);
	sprintf(msg, "q:0:0");

	nbytes = send(clipboard_id, msg, sizeof(char)*10, 0);
	if (nbytes>0) /*(!(nbytes<10))*/
	{
		memset(msg,(int) '\0' ,sizeof(char)*10);
		nbytes = recv(clipboard_id, msg, sizeof(char)*10, 0); /* deverá receber um OK */
		if(nbytes==0)
		printf("Nothing was received... Clipboard exited unexpectedly.\n");
		else
		{
			if(!strcmp(msg,"ER"))
				printf("Error. Bad request probably ?\n Closing the program anyway.\n"); /* vale a pena enviar outra vez ? */
			else if(!strcmp(msg,"OK")) /* se for OK */
				printf("Clip informed that we're quiting.\n"); 
		}
	}
	close(clipboard_id);
}