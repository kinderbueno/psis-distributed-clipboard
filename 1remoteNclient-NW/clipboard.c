#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>

#include "clipboard.h"
#include "clip_lib.h"
#include "ext_var.h"

node* head_client; /* head of the clients list */
node* head_clip; /* head of clips connecting to me list */
char* clip[10];	/* clip itself */
int in_fd=-1; /* for new clients */
int out_fd=-1;	/* for new clips */
int root_fd=-1; /* fd to the root clip*/
pthread_attr_t attr;
pthread_t root_thr;
pthread_t client_accept_thr;
pthread_rwlock_t clip_lock[10];
pthread_rwlock_t hclip_lock; 
pthread_rwlock_t hclient_lock;
pthread_rwlock_t root_lock; 
 
int main(int argc, char* argv[])
{
	struct sockaddr_in out_addr; /* for new clips */
	struct sockaddr_un in_addr; /* for new clients */ 

	int port=0, aux_port=0;

	/* auxiliar variables */
	char default_messages[10]; /* OK(ACK) and ERROR messages */ 
	int err=0, i=0;
	char act='\0';

	void (*old_handler)(int); /* não quebrar nos erros do send */
	if((old_handler=signal(SIGPIPE,SIG_IGN))==SIG_ERR)
	{
		perror("Could not handle SIGPIPE");
		exit(0);
	}

	for(i=0;i<10;i++)
		pthread_rwlock_init(&(clip_lock[i]), NULL);
	pthread_rwlock_init(&hclip_lock, NULL);
	pthread_rwlock_init(&hclient_lock, NULL);
	pthread_rwlock_init(&root_lock, NULL);

	/* inicialização do clip */
	for(i=0;i<10;i++)
		clip[i]=NULL;

	printf("c.1\n");

	pthread_attr_init(&attr);

	err = check_args(argc, argv);
	if (err<=0)
		root_fd=-1;
	else
	{
		root_fd=err;

		err = fetch_clip(root_fd);
		if (err==0)
		{
			printf("Remove clip and becoming root.\n");
			root_fd=-1;
			close(root_fd);
			/* PROTEG mudar o estado do root */
		}
	}
	/* função de inits */
	printf("aqui1\n");

	/* criar socket à escuta de clientes */
	in_fd=socket(AF_UNIX, SOCK_STREAM, 0);
	if (in_fd == -1)
	{
		perror("socket in_fd: ");
		if(root_fd!=-1)
		{
			close(root_fd);
			for(i=0;i<10;i++)
				free(clip[i]);
		}
		exit(-1);
	}
	memset(&in_addr,(int) '\0', sizeof(in_addr));
	in_addr.sun_family = AF_UNIX;
	strcpy(in_addr.sun_path, SOCK_ADDRESS); /* VER ISTO !!! */
	unlink(SOCK_ADDRESS); /* para evitar erros de bind quando o programa vai abaixo */

	err = bind(in_fd, (struct sockaddr *)&in_addr, sizeof(in_addr));
	if(err == -1){
		perror("bind in_fd: ");
		close(in_fd);
		if(root_fd!=-1)
		{
			close(root_fd);
			for(i=0;i<10;i++)
				free(clip[i]);
		}
		exit(-1);
	}
	printf("aqui2\n");
	err=listen(in_fd, 5); /* torna o socket passivo */
	if(err==-1)
	{
		perror("listen in_fd: ");
		if(root_fd!=-1)
		{
			close(root_fd);
			for(i=0;i<10;i++)
				free(clip[i]);
		}
		exit(-1);
	}

	printf("c.4\n");

	/* criar socket e defini-lo na estrutura e por o programa à escuta de clips*/
	out_fd=socket(AF_INET, SOCK_STREAM, 0);
	if (out_fd == -1)
	{
		perror("socket out_fd: ");
		close(in_fd);
		if(root_fd!=-1)
		{
			close(root_fd);
			for(i=0;i<10;i++)
				free(clip[i]);
		}
		exit(-1);
	}
	memset(&out_addr,(int) '\0',sizeof(out_addr));
	out_addr.sin_family = AF_INET;
	out_addr.sin_addr.s_addr = htonl(INADDR_ANY); 

	port=(random()%(MAXPORT-MINPORT))+MINPORT;
	while(1)
	{
		aux_port=port;
		out_addr.sin_port = htons ((u_short) aux_port); 
		err = bind(out_fd, (struct sockaddr *)&out_addr, sizeof(out_addr));
		if(err == -1)
		{
			aux_port++;
			if(aux_port>MAXPORT)
				aux_port=MINPORT;
			else if(aux_port==port)
			{
				printf("No more ports available.\n");
				perror("bind out_fd: ");
				close(in_fd);
				close(out_fd);
				if(root_fd!=-1)
				{
					close(root_fd);
					for(i=0;i<10;i++)
						free(clip[i]);
				}
				exit(-1);
			}
		}
		else
		{
			port=aux_port;
			break;
		}
	}

	/* torna o socket passivo agrupando tentativas de ligaçao ate 5 valores */
	err=listen(out_fd, 5);
	if(err==-1)
	{
		perror("listen out_fd: ");
		close(out_fd);
		close(in_fd);
		if(root_fd!=-1)
		{
			close(root_fd);
			for(i=0;i<10;i++)
				free(clip[i]);
		}
		exit(-1);
	}

	printf("c.4646\n");

	if(root_fd!=-1)  /* este clip esta ligado a outro clip */
	{
		printf("oioi: root_fd = %d\n", root_fd);
		pthread_create(&root_thr, &attr, thread_root, &root_fd);
	}
	printf("oioioioi: in_fd = %d\n", in_fd);
	pthread_create(&client_accept_thr, &attr, thread_accept, &in_fd);
	/* pthread_create(&client_accept_thr, &attr, thread_accept, &out_fd); */
	
	printf("Init done: conection to next clip, to the first client and regions syncronized.\n");
	printf("COMMANDS:\ns\t\tshows the 10 regions stored values\nq\t\tquits program\np\t\tprints the port listening to new clipboards\n");

	while(1)
	{
		printf("admin@clipboard-> ");
		memset(default_messages,(int)'\0',10*sizeof(char));
		fgets(default_messages, 10, stdin);
		sscanf(default_messages, "%c", &act); 
		if (act=='p')
		{
			printf("PORT: %d\n", port);
		}
		else if (act=='q') 
		{
			break;
		}
		else if (act=='s')
		{
            printf("\n");
            printf("CLIPBOARD: (region:size -> string)\n");
            for(i=0;i<10;i++)
            {
            	pthread_rwlock_wrlock(&clip_lock[i]);
                if (strlen(clip[i])!=0)
                        printf("%d:%lu->%s\n", i+1, strlen(clip[i]), clip[i]);
                else
                        printf("%d:%lu->\n", i+1, strlen(clip[i]));
				pthread_rwlock_unlock(&clip_lock[i]); 
            }
            printf("\n");
		}
		else
			printf("Unkown command. Try: 's' (show clip), 'p' (show port) or 'q' (quit).\n");
	}

	/* FAZER O FREE DAS LISTAS */


	pthread_rwlock_wrlock(&root_lock);
	if (root_fd!=-1)
		close(root_fd);
	pthread_rwlock_unlock(&root_lock);
	close(out_fd);
	close(in_fd);

	/* free das estruturas abertas quando exit */
	for(i=0;i<10;i++)
	{
		pthread_rwlock_wrlock(&(clip_lock[i]));
		if(clip[i]!=NULL)
			free(clip[i]);
		pthread_rwlock_unlock(&(clip_lock[i]));
	}

	for(i=0;i<10;i++)
		pthread_rwlock_destroy(&(clip_lock[i]));
	pthread_rwlock_destroy(&hclip_lock);
	pthread_rwlock_destroy(&hclient_lock);
	pthread_rwlock_destroy(&root_lock);

	pthread_attr_destroy(&attr);
		
	exit(0);
	
}
